<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#orge734538">1. The project is launched! <span class="timestamp-wrapper"><span class="timestamp">&lt;2018-03-14 mer.&gt;</span></span></a></li>
</ul>
</div>
</div>


<a id="orge734538"></a>

# The project is launched! <span class="timestamp-wrapper"><span class="timestamp">&lt;2018-03-14 mer.&gt;</span></span>

The CardView project is launched.  It aims at providing a GTK+ widget
to view a card game with a main player, other players, and a few other
board areas.

Its primary use is for the `tarot` project (The Amazing Rules Of
Tarot), a computerized adaptation of a popular card game in France
with subtle rules.

