#!/bin/sh
# deploy.sh
#
# Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
# 
#  This program is free software: you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
# 
#  This program is distributed in the hope that it will be useful, but
#  WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see
#  <http://www.gnu.org/licenses/>.

./autogen.sh || exit 1
./configure CFLAGS="-Wall -Wextra -g -O0" \
	    --enable-silent-rules=yes \
	    --enable-gtk-doc=yes \
    || exit 1
make -j 16
# Do not dist nor check!
make -j 16 install

# Then, use the installed resource compiler!
export PATH="$PATH:/usr/local/bin"
./configure CFLAGS="-Wall -Wextra -g -O0" \
	    --prefix="$PWD/public" \
	    --enable-silent-rules=yes \
    || exit 1
make -j 16 || exit 1
make -j 16 check || (cat test-suite.log && exit 1)
make -j 16 distcheck || exit 1
make -j 16 install-html || exit 1
make -j 16 install-pdf || exit 1
cp *.tar.gz public/
