#!/bin/sh
# post-commit.sh
#
# Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
# 
#  This program is free software: you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
# 
#  This program is distributed in the hope that it will be useful, but
#  WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see
#  <http://www.gnu.org/licenses/>.

CURRENT_DIR="$PWD"

tmpdir=$(mktemp -d)
mkdir "$tmpdir/original"
git archive HEAD | tar -xf - -C "$tmpdir/original"
cp -R "$tmpdir/original" "$tmpdir/rebuilt"
cd "$tmpdir/rebuilt" || exit 1

./autogen.sh || exit 1

# Re-build
./configure CFLAGS="-Wall -Wextra -g -O0" \
	    --enable-silent-rules=yes \
	    --enable-gtk-doc=yes \
	    --prefix="$tmpdir/installation" \
    || exit 1
make -j 16 indent
make -j 16 dist || exit 1
make -j 16 || exit 1
make -j 16 check || exit 1
# Check that there are no cycles in the Makefile.
chmod -R ugo-w ./* || exit 1
chmod u+w *.tar.gz || exit 1
make -j 16 distcheck || exit 1

cd "$tmpdir/original"
for f in $(find . -type f) ; do
    case $f in
	*.po|*.pot)
	    echo "Ignoring the POT file $f.";;
	*.md)
	    echo "Ignoring the org-generated Markdown file $f.";;
	*)
	    echo "Checking that $f is not ignored..."
	    grep "$f" .gitignore && exit 1
	    grep "$f" ../rebuilt/.gitignore && exit 1
	    echo "Checking that $f is distributed..."
	    for archive in $tmpdir/rebuilt/*.tar.gz
	    do
		(tar tf $archive | grep "$f") || exit 1
	    done
	    echo "Comparing $f..."
	    diff $f ../rebuilt/$f || exit 1;;
    esac
done

cd "$CURRENT_DIR" || exit 1
chmod -R u+w "$tmpdir" || exit 1
rm -r "$tmpdir" || exit 1

echo "** OK **"
