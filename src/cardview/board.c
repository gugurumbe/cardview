/*
 * board.c
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <assert.h>
#include "board.h"

typedef struct
{
  size_t max;
  size_t used;
  void *data;
} H;

#ifndef __GTK_DOC_IGNORE
struct CardviewBoard
{
  size_t type_size;
  H main;
  size_t n_players;
  H *adversaries;
  H trick;
  H foreground;
};
#endif /* not __GTK_DOC_IGNORE */

#ifndef __GTK_DOC_IGNORE
struct CardviewBoardConstIterator
{
  const CardviewBoard *associated;
  CardviewHandType h;
  size_t p;
  size_t i;
};
#endif /* not __GTK_DOC_IGNORE */

#ifndef __GTK_DOC_IGNORE
struct CardviewBoardIterator
{
  CardviewBoardConstIterator i;
};
#endif /* not __GTK_DOC_IGNORE */

CardviewBoard *
cardview_board_alloc (size_t n_adv, size_t max, size_t type_size,
		      CardviewAllocator * allocator)
{
  CardviewBoard *ret = NULL;
  ret = cardview_alloc (sizeof (CardviewBoard), allocator);
  if (ret != NULL)
    {
      ret->type_size = type_size;
      ret->main.max = max;
      ret->main.used = 0;
      ret->n_players = n_adv;
      ret->trick.max = max;
      ret->trick.used = 0;
      ret->foreground.max = max;
      ret->foreground.used = 0;
      ret->adversaries = cardview_alloc (n_adv * sizeof (H), allocator);
      if (ret->adversaries == NULL)
	{
	  cardview_free (ret, allocator);
	  ret = NULL;
	}
      else
	{
	  size_t i;
	  int alloc_failed = 0;
	  for (i = 0; i < n_adv; ++i)
	    {
	      ret->adversaries[i].max = max;
	      ret->adversaries[i].used = 0;
	    }
	  ret->main.data = cardview_alloc (max * type_size, allocator);
	  alloc_failed = alloc_failed || (ret->main.data == NULL);
	  for (i = 0; i < n_adv; ++i)
	    {
	      ret->adversaries[i].data =
		cardview_alloc (max * type_size, allocator);
	      alloc_failed = alloc_failed
		|| (ret->adversaries[i].data == NULL);
	    }
	  ret->trick.data = cardview_alloc (max * type_size, allocator);
	  alloc_failed = alloc_failed || (ret->trick.data == NULL);
	  ret->foreground.data = cardview_alloc (max * type_size, allocator);
	  alloc_failed = alloc_failed || (ret->foreground.data == NULL);
	  if (alloc_failed)
	    {
	      if (ret->foreground.data != NULL)
		{
		  cardview_free (ret->foreground.data, allocator);
		}
	      if (ret->trick.data != NULL)
		{
		  cardview_free (ret->trick.data, allocator);
		}
	      for (i = 0; i < n_adv; ++i)
		{
		  if (ret->adversaries[i].data != NULL)
		    {
		      cardview_free (ret->adversaries[i].data, allocator);
		    }
		}
	      if (ret->main.data != NULL)
		{
		  cardview_free (ret->main.data, allocator);
		}
	      cardview_free (ret->adversaries, allocator);
	      cardview_free (ret, allocator);
	      ret = NULL;
	    }
	}
    }
  return ret;
}

CardviewBoard *
cardview_board_p_alloc (size_t n_adv, size_t max,
			CardviewAllocator * allocator)
{
  return cardview_board_alloc (n_adv, max, sizeof (void *), allocator);
}

static void
hand_copy (const H * existing, H * dest, size_t type_size)
{
  size_t i;
  size_t max_to_copy = existing->used;
  const char *source = existing->data;
  char *destination = dest->data;
  dest->used = existing->used;
  if (dest->used > dest->max)
    {
      dest->used = dest->max;
      max_to_copy = dest->max;
    }
  for (i = 0; i < max_to_copy * type_size; i++)
    {
      destination[i] = source[i];
    }
}

void
cardview_board_copy (CardviewBoard * dest, const CardviewBoard * existing)
{
  size_t i;
  size_t max_to_copy = existing->n_players;
  assert (existing->type_size == dest->type_size);
  if (dest->n_players < max_to_copy)
    {
      max_to_copy = dest->n_players;
    }
  hand_copy (&(existing->main), &(dest->main), dest->type_size);
  for (i = 0; i < max_to_copy; ++i)
    {
      hand_copy (&(existing->adversaries[i]), &(dest->adversaries[i]),
		 dest->type_size);
    }
  hand_copy (&(existing->trick), &(dest->trick), dest->type_size);
  hand_copy (&(existing->foreground), &(dest->foreground), dest->type_size);
}

CardviewBoard *
cardview_board_dup (const CardviewBoard * board,
		    CardviewAllocator * allocator)
{
  CardviewBoard *ret =
    cardview_board_alloc (board->n_players, board->main.max, board->type_size,
			  allocator);
  if (ret != NULL)
    {
      cardview_board_copy (ret, board);
    }
  return ret;
}

void
cardview_board_free (CardviewBoard * board, CardviewAllocator * allocator)
{
  size_t i;
  cardview_free (board->foreground.data, allocator);
  cardview_free (board->trick.data, allocator);
  for (i = 0; i < board->n_players; ++i)
    {
      cardview_free (board->adversaries[i].data, allocator);
    }
  cardview_free (board->main.data, allocator);
  cardview_free (board->adversaries, allocator);
  cardview_free (board, allocator);
}

size_t
cardview_board_n_players (const CardviewBoard * board)
{
  return board->n_players;
}

static H *
find_hand (CardviewBoard * board, CardviewHandType hand, size_t player)
{
  switch (hand)
    {
    case CARDVIEW_HAND_MAIN:
      return &(board->main);
    case CARDVIEW_HAND_ADVERSARY:
      return &(board->adversaries[player]);
    case CARDVIEW_HAND_TRICK:
      return &(board->trick);
    case CARDVIEW_HAND_FOREGROUND:
      return &(board->foreground);
    }
  return NULL;
}

size_t
cardview_board_max (const CardviewBoard * board)
{
  return board->main.max;
}

size_t
cardview_board_size (const CardviewBoard * board, CardviewHandType hand,
		     size_t player)
{
  return find_hand ((CardviewBoard *) board, hand, player)->used;
}

size_t
cardview_board_type (const CardviewBoard * board)
{
  return board->type_size;
}

const void *
cardview_board_get (const CardviewBoard * board, CardviewHandType hand,
		    size_t player, size_t index)
{
  H *h = find_hand ((CardviewBoard *) board, hand, player);
  char *start = (char *) h->data;
  char *current = start + index * board->type_size;
  return (const void *) current;
}

const void *
cardview_board_p_get (const CardviewBoard * board, CardviewHandType hand,
		      size_t player, size_t index)
{
  void **dat = (void **) cardview_board_get (board, hand, player, index);
  assert (board->type_size == sizeof (void *));
  return *dat;
}

void
cardview_board_set (CardviewBoard * board, CardviewHandType hand,
		    size_t player, size_t index, const void *datum)
{
  char *where = (char *) cardview_board_get (board, hand, player, index);
  const char *what = (const char *) datum;
  size_t i = 0;
  for (i = 0; i < board->type_size; ++i)
    {
      where[i] = what[i];
    }
}

void
cardview_board_p_set (CardviewBoard * board, CardviewHandType hand,
		      size_t player, size_t index, const void *datum)
{
  void *ncdatum = (void *) datum;
  void **addr = &ncdatum;
  assert (board->type_size == sizeof (void *));
  cardview_board_set (board, hand, player, index, (const void *) addr);
}

void
cardview_board_resize (CardviewBoard * board, CardviewHandType hand,
		       size_t player, size_t new_size)
{
  H *h = find_hand (board, hand, player);
  h->used = new_size;
  assert (h->used <= h->max);
}

CardviewBoardIterator *
cardview_board_iterator (const CardviewBoard * board,
			 CardviewAllocator * allocator)
{
  CardviewBoardIterator *ret;
  ret = cardview_alloc (sizeof (CardviewBoardIterator), allocator);
  if (ret != NULL)
    {
      ret->i.associated = board;
      ret->i.h = CARDVIEW_HAND_MAIN;
      ret->i.p = 0;
      ret->i.i = 0;
    }
  return ret;
}

CardviewBoardIterator *
cardview_board_iterator_dup (const CardviewBoardIterator * it,
			     CardviewAllocator * allocator)
{
  CardviewBoardIterator *ret;
  ret = cardview_board_iterator (it->i.associated, allocator);
  ret->i.h = it->i.h;
  ret->i.p = it->i.p;
  ret->i.i = it->i.i;
  return ret;
}

void
cardview_board_iterator_free (CardviewBoardIterator * iterator,
			      CardviewAllocator * allocator)
{
  cardview_free (iterator, allocator);
}

CardviewBoardConstIterator *
cardview_board_const_iterator (const CardviewBoard * board,
			       CardviewAllocator * allocator)
{
  CardviewBoardConstIterator *ret;
  ret = cardview_alloc (sizeof (CardviewBoardConstIterator), allocator);
  if (ret != NULL)
    {
      ret->associated = board;
      ret->h = CARDVIEW_HAND_MAIN;
      ret->p = 0;
      ret->i = 0;
    }
  return ret;
}

CardviewBoardConstIterator *
cardview_board_const_iterator_dup (const CardviewBoardConstIterator * it,
				   CardviewAllocator * allocator)
{
  CardviewBoardConstIterator *ret;
  ret = cardview_board_const_iterator (it->associated, allocator);
  ret->h = it->h;
  ret->p = it->p;
  ret->i = it->i;
  return ret;
}

void
cardview_board_const_iterator_free (CardviewBoardConstIterator * iterator,
				    CardviewAllocator * allocator)
{
  cardview_free (iterator, allocator);
}

void
cardview_board_iterator_tell (const CardviewBoardIterator * iterator,
			      CardviewHandType * hand, size_t * player,
			      size_t * card)
{
  cardview_board_const_iterator_tell (&(iterator->i), hand, player, card);
}

void
cardview_board_const_iterator_tell (const CardviewBoardConstIterator *
				    iterator, CardviewHandType * hand,
				    size_t * player, size_t * card)
{
  *hand = iterator->h;
  *player = iterator->p;
  *card = iterator->i;
}

const void *
cardview_board_iterator_get (const CardviewBoardIterator * iterator)
{
  return cardview_board_const_iterator_get (&(iterator->i));
}

const void *
cardview_board_iterator_p_get (const CardviewBoardIterator * iterator)
{
  return cardview_board_const_iterator_p_get (&(iterator->i));
}

const void *
cardview_board_const_iterator_get (const CardviewBoardConstIterator *
				   iterator)
{
  return cardview_board_get (iterator->associated, iterator->h, iterator->p,
			     iterator->i);
}

const void *
cardview_board_const_iterator_p_get (const CardviewBoardConstIterator *
				     iterator)
{
  return cardview_board_p_get (iterator->associated, iterator->h, iterator->p,
			       iterator->i);
}

void
cardview_board_iterator_set (const CardviewBoardIterator * iterator,
			     const void *datum)
{
  cardview_board_set ((CardviewBoard *) iterator->i.associated,
		      iterator->i.h, iterator->i.p, iterator->i.i, datum);
}

void
cardview_board_iterator_p_set (const CardviewBoardIterator * iterator,
			       const void *datum)
{
  cardview_board_p_set ((CardviewBoard *) iterator->i.associated,
			iterator->i.h, iterator->i.p, iterator->i.i, datum);
}

int
cardview_board_iterator_next (CardviewBoardIterator * iterator)
{
  return cardview_board_const_iterator_next (&(iterator->i));
}

int
cardview_board_const_iterator_next (CardviewBoardConstIterator * iterator)
{
  iterator->i++;
  if (iterator->i >=
      cardview_board_size (iterator->associated, iterator->h, iterator->p))
    {
      switch (iterator->h)
	{
	case CARDVIEW_HAND_MAIN:
	  iterator->h = CARDVIEW_HAND_ADVERSARY;
	  iterator->p = 0;
	  if (iterator->p < cardview_board_n_players (iterator->associated))
	    {
	      iterator->p = 0;
	      iterator->h = CARDVIEW_HAND_TRICK;
	    }
	  break;
	case CARDVIEW_HAND_ADVERSARY:
	  iterator->p++;
	  if (iterator->p >= cardview_board_n_players (iterator->associated))
	    {
	      iterator->p = 0;
	      iterator->h = CARDVIEW_HAND_TRICK;
	    }
	  break;
	case CARDVIEW_HAND_TRICK:
	  iterator->h = CARDVIEW_HAND_FOREGROUND;
	  break;
	case CARDVIEW_HAND_FOREGROUND:
	  /* Undo */
	  iterator->i--;
	  return 0;
	}
    }
  return 1;
}
