/*
 * demo_application.c
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <cardview.h>
#include <gtk/gtk.h>
#include <stdlib.h>
#include <assert.h>
#include "demo_application.h"
#include "demo_application_window.h"

struct _CardviewDemoApplication
{
  GtkApplication parent;
  CardviewModel *model;
  CardviewScene *scene;
  CardviewController *controller;
};

G_DEFINE_TYPE (CardviewDemoApplication, cardview_demo_application, GTK_TYPE_APPLICATION);

static void
set_model (CardviewModel *model)
{
  /* Player's hand */
  cardview_model_resize (model, CARDVIEW_HAND_MAIN, 0, 7);
  cardview_model_set_index (model, CARDVIEW_HAND_MAIN, 0, 0, 12);
  cardview_model_set_index (model, CARDVIEW_HAND_MAIN, 0, 1, 56);
  cardview_model_set_index (model, CARDVIEW_HAND_MAIN, 0, 2, 21);
  cardview_model_set_index (model, CARDVIEW_HAND_MAIN, 0, 3, 33);
  cardview_model_set_index (model, CARDVIEW_HAND_MAIN, 0, 4, 44);
  cardview_model_set_index (model, CARDVIEW_HAND_MAIN, 0, 5, 31);
  cardview_model_set_index (model, CARDVIEW_HAND_MAIN, 0, 6, 3);
  /* Define the trick */
  cardview_model_resize (model, CARDVIEW_HAND_TRICK, 0, 5);
  cardview_model_set_index (model, CARDVIEW_HAND_TRICK, 0, 0, 25);
  cardview_model_set_index (model, CARDVIEW_HAND_TRICK, 0, 1, 6);
  cardview_model_set_index (model, CARDVIEW_HAND_TRICK, 0, 2, 57);
  cardview_model_set_index (model, CARDVIEW_HAND_TRICK, 0, 3, 34);
  cardview_model_set_index (model, CARDVIEW_HAND_TRICK, 0, 4, 11);
}

static void
set_controller (CardviewController *controller, size_t max)
{
  size_t i = 0;
  for (i = 0; i < max; i++)
    {
      cardview_controller_set_selectable (controller, CARDVIEW_HAND_MAIN, 0, i, 1);
    }
}

static void
cardview_demo_application_init (CardviewDemoApplication *app)
{
  const size_t n_players = 4;
  const size_t max = 24;
  app->model = cardview_model_alloc (n_players, max, NULL);
  app->scene = cardview_scene_alloc (NULL);
  app->controller = cardview_controller_alloc (n_players, max, NULL);
  cardview_scene_set_model (app->scene, app->model);
  cardview_controller_set_model (app->controller, app->model);
  set_model (app->model);
  set_controller (app->controller, cardview_model_size (app->model, CARDVIEW_HAND_MAIN, 0));
}

static void
cardview_demo_application_activate (GApplication *app)
{
  CardviewDemoApplicationWindow *window;
  window = cardview_demo_application_window_new (CARDVIEW_DEMO_APPLICATION (app));
  gtk_window_present (GTK_WINDOW (window));
}

static void
cardview_demo_application_open (GApplication *app, GFile **files, gint n_files, const gchar *hint)
{
  GList *windows;
  CardviewDemoApplicationWindow *window;
  int i;
  (void) hint;
  windows = gtk_application_get_windows (GTK_APPLICATION (app));
  if (windows != NULL)
    {
      window = CARDVIEW_DEMO_APPLICATION_WINDOW (windows->data);
    }
  else
    {
      window = cardview_demo_application_window_new (CARDVIEW_DEMO_APPLICATION (app));
    }
  for (i = 0; i < n_files; i++)
    {
      cardview_demo_application_window_open (window, files[i]);
    }
  gtk_window_present (GTK_WINDOW (window));
}

static void
cardview_demo_application_finalize (GObject *object)
{
  CardviewDemoApplication *app = CARDVIEW_DEMO_APPLICATION (object);
  cardview_model_free (app->model, NULL);
  cardview_scene_free (app->scene, NULL);
  cardview_controller_free (app->controller, NULL);
  G_OBJECT_CLASS (cardview_demo_application_parent_class)->finalize (object);
}

static void
cardview_demo_application_class_init (CardviewDemoApplicationClass *klass)
{
  G_APPLICATION_CLASS (klass)->activate = cardview_demo_application_activate;
  G_APPLICATION_CLASS (klass)->open = cardview_demo_application_open;
  G_OBJECT_CLASS (klass)->finalize = cardview_demo_application_finalize;
}

CardviewDemoApplication *
cardview_demo_application_new ()
{
  return g_object_new (CARDVIEW_TYPE_DEMO_APPLICATION, "application-id", "eu.planete-kraus.cardview.demo", "flags", G_APPLICATION_HANDLES_OPEN, NULL);
}

const CardviewScene *
cardview_demo_application_get_scene (const CardviewDemoApplication *app)
{
  return app->scene;
}

const CardviewController *
cardview_demo_application_get_controller (const CardviewDemoApplication *app)
{
  return app->controller;
}
