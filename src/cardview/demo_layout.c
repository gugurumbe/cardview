/*
 * demo_layout.c: generate a SVG that shows an example of the layout.
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#define _GNU_SOURCE
#include <cardview.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>

static CardviewAllocator allocator;

#define N_MAX 100

static void
print_line (const CardviewRendering * card)
{
  printf ("\
  <!-- z=%f -->\n\
  <g id=\"%d\" transform=\"translate(%f, %f)\">\n\
    <g transform=\"rotate(%f)\">\n\
      <g transform=\"scale(%f)\">\n\
	<use xlink:href=\"#card%d\"/>\n\
      </g>\n\
    </g>\n\
  </g>\n", card->z, (int) card->index, card->x, -card->y, -180 * card->angle / M_PI, card->scale, (int) card->index);
}

static void
rand_hand (CardviewModel * model, CardviewHandType hand, size_t player,
	   size_t n)
{
  size_t i;
  cardview_model_resize (model, hand, player, n);
  for (i = 0; i < n; i++)
    {
      cardview_model_set_index (model, hand, player, i,
				((unsigned int) rand ()) % 2);
    }
}

static void
rand_model (CardviewModel * model)
{
  size_t n_players = cardview_model_n_players (model);
  size_t i;
  rand_hand (model, CARDVIEW_HAND_MAIN, 0, 15);
  for (i = 0; i < n_players; ++i)
    {
      rand_hand (model, CARDVIEW_HAND_ADVERSARY, i, 15);
    }
  rand_hand (model, CARDVIEW_HAND_TRICK, 0, n_players + 1);
  rand_hand (model, CARDVIEW_HAND_FOREGROUND, 0, 3);
}

static int
run ()
{
  CardviewModel *model = cardview_model_alloc (4, 24, &allocator);
  CardviewScene *scene = cardview_scene_alloc (&allocator);
  CardviewRendering rendering[N_MAX];
  size_t used;
  size_t i;
  cardview_scene_set_model (scene, model);
  cardview_scene_set_view_size (scene, 640, 480);
  cardview_scene_set_card_size (scene, 0.66, 1);
  rand_model (model);
  printf ("\
<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n\
<svg\n\
   xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n\
   xmlns:cc=\"http://creativecommons.org/ns#\"\n\
   xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n\
   xmlns:svg=\"http://www.w3.org/2000/svg\"\n\
   xmlns:xlink=\"http://www.w3.org/1999/xlink\"\n\
   xmlns=\"http://www.w3.org/2000/svg\"\n\
   id=\"svg8\"\n\
   version=\"1.1\"\n\
   viewBox=\"-320 -240 640 480\"\n\
   width=\"640\"\n\
   height=\"480\">\n\
  <defs id=\"defs\">\n\
    <g id=\"card0\">\n\
      <rect\n\
	x=\"-0.33\"\n\
	y=\"-0.5\"\n\
	width=\"0.66\"\n\
	height=\"1\"\n\
	fill=\"white\"\n\
	stroke=\"black\"\n\
	stroke-width=\"0.01\"\n\
	rx=\"0.05\"\n\
	ry=\"0.05\"/>\n\
    </g>\n\
    <g id=\"card1\">\n\
      <rect\n\
	x=\"-0.33\"\n\
	y=\"-0.5\"\n\
	width=\"0.66\"\n\
	height=\"1\"\n\
	fill=\"red\"\n\
	stroke=\"black\"\n\
	stroke-width=\"0.01\"\n\
	rx=\"0.05\"\n\
	ry=\"0.05\"/>\n\
    </g>\n\
  </defs>\n\
  <rect id=\"background\"\n\
        x=\"-320\" y=\"-240\" width=\"640\" height=\"480\"\n\
        fill=\"green\" />\n\
");
  used = cardview_scene_positions (scene, N_MAX, rendering);
  assert (used < N_MAX);
  for (i = 0; i < used; ++i)
    {
      print_line (&(rendering[i]));
    }
  printf ("</svg>\n");
  cardview_scene_free (scene, &allocator);
  cardview_model_free (model, &allocator);
  return 0;
}

static void *
my_alloc (void *user_data, size_t size)
{
  (void) user_data;
  void *ret = malloc (size);
  if (ret == NULL)
    {
      fprintf (stderr, "Dynamic memory exhausted.\n");
      exit (1);
    }
  return ret;
}

static void
my_free (void *user_data, void *mem)
{
  (void) user_data;
  free (mem);
}

int
main ()
{
  allocator.user_data = NULL;
  allocator.alloc = my_alloc;
  allocator.free = my_free;
  return run ();
}
