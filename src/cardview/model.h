/*
 * model.h
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_CARDVIEW_MODEL_INCLUDED
#define H_CARDVIEW_MODEL_INCLUDED

#include <stddef.h>
#include <cardview/allocator.h>

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */

#ifndef __GTK_DOC_IGNORE__
  struct CardviewModel;
  struct CardviewModelIterator;
#endif

  /**
   * CardviewModel:
   *
   * A board of cards.
   */
  typedef struct CardviewModel CardviewModel;

  /**
   * CardviewHandType:
   *
   * Type of hand.
   *
   * CARDVIEW_HAND_MAIN: Refers to the main card set. 
   * CARDVIEW_HAND_ADVERSARY: Refers to the card of another player.
   * CARDVIEW_HAND_TRICK: Refers to the cards in the trick.
   * CARDVIEW_HAND_FOCUS: Refers to the cards in the additional
   * cards in the center.
   */
  typedef enum
  {
    CARDVIEW_HAND_MAIN,
    CARDVIEW_HAND_ADVERSARY,
    CARDVIEW_HAND_TRICK,
    CARDVIEW_HAND_FOREGROUND
  } CardviewHandType;

  /**
   * cardview_model_alloc:
   *
   * Create a new model.
   *
   * n_other_players: the number of other players.
   * max: the maximum number of cards per hand.
   * allocator: (value NULL): unused in gobject.
   * Returns: (transfer full): a new model.
   */
  CardviewModel *cardview_model_alloc (size_t n_other_players, size_t max,
				       CardviewAllocator * allocator);

  /**
   * cardview_model_copy: (skip):
   *
   * Make a copy of an existing model.  This operation is costly.
   *
   * existing: an existing model.
   * dest: an allocated model which may have different dimensions.
   */
  void cardview_model_copy (CardviewModel * dest,
			    const CardviewModel * existing);

  /**
   * cardview_model_dup:
   *
   * Allocate and copy a model.
   *
   * model: the model.
   * allocator: (value NULL): unused with GLib.
   * Returns: an allocated copy of the model.
   */
  CardviewModel *cardview_model_dup (const CardviewModel * model,
				     CardviewAllocator * allocator);

  /**
   * cardview_model_free: (skip):
   *
   * Destroy a model.
   *
   * model: the model to destroy.
   * allocator: (value NULL): unused in gobject.
   */
  void cardview_model_free (CardviewModel * model,
			    CardviewAllocator * allocator);

  /**
   * cardview_model_n_players:
   *
   * Get the number of players in the model.
   *
   * model: the model.
   * Returns: the number of players in the model.
   */
  size_t cardview_model_n_players (const CardviewModel * model);

  /**
   * cardview_model_max:
   *
   * Get the maximum number of cards per hand in the model.
   *
   * model: the model.
   * Returns: the maximum number of cards per hand in the model.
   */
  size_t cardview_model_max (const CardviewModel * model);

  /**
   * cardview_model_size:
   *
   * Get the size of a hand.
   *
   * model: the model.
   * hand: which hand.
   * player: which adversary.
   * Returns: the number of cards in this hand.
   */
  size_t cardview_model_size (const CardviewModel * model,
			      CardviewHandType hand, size_t player);

  /**
   * cardview_model_get_index:
   *
   * Get the card index of a card.
   *
   * model: the model.
   * hand: which hand.
   * player: which adversary.
   * i: which card.
   * Returns: the index of the card.
   */
  unsigned int cardview_model_get_index (const CardviewModel * model,
					 CardviewHandType hand, size_t player,
					 size_t i);

  /**
   * cardview_model_get_selected:
   *
   * Check whether a card is selected.
   *
   * model: the model.
   * hand: which hand.
   * player: which adversary.
   * i: which card.
   * Returns: (type bool): whether the card is selected.
   */
  int cardview_model_get_selected (const CardviewModel * model,
				   CardviewHandType hand, size_t player,
				   size_t i);

  /**
   * cardview_model_get_pointed:
   *
   * Get the current pointed card.
   *
   * model: the model.
   * hand: which hand.
   * player: which adversary.
   * i: which card.
   * Returns: (type bool): whether there is a pointed card.
   */
  int cardview_model_get_pointed (const CardviewModel * model,
				  CardviewHandType * hand, size_t * player,
				  size_t * i);

  /**
   * cardview_model_set_index:
   *
   * Define the index of a card.
   *
   * model: the model.
   * hand: which hand.
   * player: which player.
   * i: which card.
   * index: the new index.
   */
  void cardview_model_set_index (CardviewModel * model, CardviewHandType hand,
				 size_t player, size_t i, unsigned int index);

  /**
   * cardview_model_set_selected:
   *
   * Define whether a card is selected.
   *
   * model: the model.
   * hand: which hand.
   * player: which player.
   * i: which card.
   * selected: (type bool): whether it is selected.
   */
  void cardview_model_set_selected (CardviewModel * model,
				    CardviewHandType hand, size_t player,
				    size_t i, int selected);
  /**
   * cardview_model_set_pointed:
   *
   * Define the pointed card.
   *
   * model: the model.
   * hand: which hand.
   * player: which player.
   * i: which card.
   */
  void cardview_model_set_pointed (CardviewModel * model,
				   CardviewHandType hand, size_t player,
				   size_t i);
  /**
   * cardview_model_unset_pointed:
   *
   * Unset the pointed card.
   *
   * model: the model.
   */
  void cardview_model_unset_pointed (CardviewModel * model);

  /**
   * cardview_model_resize:
   *
   * Resize a hand.
   *
   * model: the model.
   * hand: which hand.
   * player: which player.
   * new_size: the new size, it must be at most card_view_model_max (model).
   */
  void cardview_model_resize (CardviewModel * model, CardviewHandType hand,
			      size_t player, size_t new_size);

  /**
   * CardviewModelIterator:
   *
   * An iterator over a model.
   */
  typedef struct CardviewModelIterator CardviewModelIterator;

  /**
   * cardview_model_iterator:
   *
   * Obtain an iterator over a model.
   *
   * model: the model.
   * allocator: (value null): the allocator to allocate the model.
   * Returns a new iterator.
   */
  CardviewModelIterator *cardview_model_iterator (const CardviewModel * model,
						  CardviewAllocator *
						  allocator);

  /**
   * cardview_model_iterator_dup: (skip):
   *
   * Copy an iterator.
   *
   * iterator: the source.
   * allocator: (value null): unused with glib.
   * Returns: an iterator at the same position.
   */
  CardviewModelIterator *cardview_model_iterator_dup (const
						      CardviewModelIterator *
						      iterator,
						      CardviewAllocator *
						      allocator);

  /**
   * cardview_model_iterator_free: (skip):
   *
   * Free an iterator.
   *
   * iterator: the iterator.
   * allocator: (value null): unused with glib.
   */
  void cardview_model_iterator_free (CardviewModelIterator * iterator,
				     CardviewAllocator * allocator);

  /**
   * cardview_model_iterator_tell:
   *
   * Get the position of the iterator.
   *
   * iterator: the iterator.
   * hand: which hand it visiting.
   * player: which adversary.
   * i: which card.
   */
  void cardview_model_iterator_tell (const CardviewModelIterator * iterator,
				     CardviewHandType * hand, size_t * player,
				     size_t * i);

  /**
   * cardview_model_iterator_get_index:
   *
   * Get the index of the current card.
   *
   * iterator: the iterator.
   * Returns: the index of the current card.
   */
  unsigned int cardview_model_iterator_get_index (const CardviewModelIterator
						  * iterator);

  /**
   * cardview_model_iterator_get_selected:
   *
   * Get whether the current card is selected.
   *
   * iterator: the iterator.
   * Returns: (type bool): whether the current card is selected.
   */
  int cardview_model_iterator_get_selected (const CardviewModelIterator
					    * iterator);

  /**
   * cardview_model_iterator_next:
   *
   * Go to the next card.
   *
   * iterator: the iterator.
   * Returns: (type bool): whether there is a card after moving.
   */
  int cardview_model_iterator_next (CardviewModelIterator * iterator);

#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif				/* H_CARDVIEW_MODEL_INCLUDED */
