/*
 * board.h
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_CARDVIEW_BOARD_INCLUDED
#define H_CARDVIEW_BOARD_INCLUDED

#include <stddef.h>
#include <cardview/model.h>

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */

#ifndef __GTK_DOC_IGNORE__
  struct CardviewBoard;
  struct CardviewBoardIterator;
  struct CardviewBoardConstIterator;
#endif				/* not __GTK_DOC_IGNORE__ */

  /**
   * CardviewBoard:
   *
   * An associative array which index space is the hands of players.
   */
  typedef struct CardviewBoard CardviewBoard;

  /**
   * cardview_board_alloc: (skip):
   *
   * Make a new board.
   *
   * n_adversaries: the number of adversaries.
   * allocator: (value null): unused for glib.
   * max: the maximum number of items that may be stored in each hand.
   * type_size: (value sizeof(void *)): the number of bytes to reserve
   * for each object.  This value should be properly aligned.  With
   * glib, we only store pointers.
   * Returns: a fresh board.
   */
  CardviewBoard *cardview_board_alloc (size_t n_adversaries, size_t max,
				       size_t type_size,
				       CardviewAllocator * allocator);

  /**
   * cardview_board_p_alloc:
   *
   * Create a new board, which objects are pointers.
   *
   * n_adversaries: the number of adversaries.
   * allocator: (value null): unused for glib.
   * max: the maximum number of items that may be stored in each hand.
   * Returns: a fresh board.
   */
  CardviewBoard *cardview_board_p_alloc (size_t n_adversaries, size_t max,
					 CardviewAllocator * allocator);

  /**
   * cardview_board_copy:
   *
   * Make a shallow copy of an existing board.  They must be both of
   * the same type, however they can have different number of players
   * / cards per hand.
   *
   * existing: an existing board.
   * dest: an allocated board which may have different sizes.
   */
  void cardview_board_copy (CardviewBoard * dest,
			    const CardviewBoard * existing);

  /**
   * cardview_board_dup:
   *
   * Allocate a fresh copy.
   *
   * board: the existing board.
   * allocator: (value NULL): Unused with GLib.
   * Returns: a fresh copy.
   */
  CardviewBoard *cardview_board_dup (const CardviewBoard * board,
				     CardviewAllocator * allocator);

  /**
   * cardview_board_free: (skip):
   *
   * Free a board.
   *
   * board: the board.
   * allocator: (value null): unused with glib.
   */
  void cardview_board_free (CardviewBoard * board,
			    CardviewAllocator * allocator);

  /**
   * cardview_board_n_players:
   *
   * Get the number of players in a board.
   *
   * board: the board.
   * Returns: the number of adversaries in the board.
   */
  size_t cardview_board_n_players (const CardviewBoard * board);

  /**
   * cardview_board_max:
   *
   * Get the maximum number of items per hand.
   *
   * board: the board.
   * Returns: the maximum number of items per hand.
   */
  size_t cardview_board_max (const CardviewBoard * board);

  /**
   * cardview_board_size:
   *
   * Get the number of items used in a specific hand.
   *
   * board: the board.
   * hand: which hand.
   * player: which player.
   * Returns: the number of items used in this hand.
   */
  size_t cardview_board_size (const CardviewBoard * board,
			      CardviewHandType hand, size_t player);

  /**
   * cardview_board_type: (skip):
   *
   * Get the type of the datum.
   * 
   * board: the board.
   * Returns: the size of the type.
   */
  size_t cardview_board_type (const CardviewBoard * board);

  /**
   * cardview_board_get: (skip):
   *
   * Access the board at a specific location.
   *
   * board: the board.
   * hand: the hand.
   * player: the player.
   * index: the card index.
   * Returns: the datum associated with this hand.
   */
  const void *cardview_board_get (const CardviewBoard * board,
				  CardviewHandType hand, size_t player,
				  size_t index);

  /**
   * cardview_board_p_get:
   *
   * Access the board at a specific location.
   *
   * board: the board.
   * hand: the hand.
   * player: the player.
   * index: the card index.
   * Returns: the pointer associated with this hand.
   */
  const void *cardview_board_p_get (const CardviewBoard * board,
				    CardviewHandType hand, size_t player,
				    size_t index);

  /**
   * cardview_board_set: (skip):
   *
   * Define the value at a specific location.
   *
   * board: the board.
   * hand: the hand.
   * player: the player.
   * index: the card index.
   * datum: the value to store.
   */
  void cardview_board_set (CardviewBoard * board, CardviewHandType hand,
			   size_t player, size_t index, const void *datum);

  /**
   * cardview_board_p_set:
   *
   * Define the value at a specific location.
   *
   * board: the board.
   * hand: the hand.
   * player: the player.
   * index: the card index.
   * datum: the pointer to store.
   */
  void cardview_board_p_set (CardviewBoard * board, CardviewHandType hand,
			     size_t player, size_t index, const void *datum);

  /**
   * cardview_board_resize:
   *
   * Define the size of a hand.
   *
   * The size must be at most the max value used in the constructor.
   */
  void cardview_board_resize (CardviewBoard * board, CardviewHandType hand,
			      size_t player, size_t new_size);

  /**
   * CardviewBoardIterator:
   *
   * A read/write iterator over a board.
   */
  typedef struct CardviewBoardIterator CardviewBoardIterator;

  /**
   * CardviewBoardConstIterator:
   *
   * A read-only iterator over a board.
   */
  typedef struct CardviewBoardConstIterator CardviewBoardConstIterator;

  /**
   * cardview_board_iterator:
   *
   * Obtain a read/write iterator over a board.
   *
   * The board is used as constant, since the creation of an iterator
   * does not affect it in any way.  It is your responsibility to not
   * create a read/write iterator when given a board you should not
   * update.  It's not as if we were doing C++.  Boo for C++.
   *
   * board: the board.
   * allocator: (value null): unused with glib.
   * Returns: a new read/write iterator.
   */
  CardviewBoardIterator *cardview_board_iterator (const CardviewBoard *
						  board,
						  CardviewAllocator *
						  allocator);

  /**
   * cardview_board_iterator_free: (skip):
   *
   * Free the iterator.
   *
   * iterator: the iterator.
   * allocator: (value null): unused with glib.
   */
  void cardview_board_iterator_free (CardviewBoardIterator * iterator,
				     CardviewAllocator * allocator);

  /**
   * cardview_board_iterator_dup: (skip):
   *
   * Copy an iterator.
   *
   * iterator: the source.
   * allocator: (value null): unused with glib.
   * Returns: a copy.
   */
  CardviewBoardIterator *cardview_board_iterator_dup (const
						      CardviewBoardIterator
						      * iterator,
						      CardviewAllocator *
						      allocator);

  /**
   * cardview_board_const_iterator:
   *
   * Obtain a read-only iterator over a board.
   *
   * board: the board.
   * allocator: the allocator.
   * Returns: a new read-only iterator over board.
   */
  CardviewBoardConstIterator *cardview_board_const_iterator (const
							     CardviewBoard *
							     board,
							     CardviewAllocator
							     * allocator);

  /**
   * cardview_board_const_iterator_dup: (skip):
   *
   * Copy an iterator.
   *
   * iterator: the source.
   * allocator: (value NULL): unused with glib.
   * Returns: a copy.
   */
  CardviewBoardConstIterator *cardview_board_const_iterator_dup (const
								 CardviewBoardConstIterator
								 * iterator,
								 CardviewAllocator
								 * allocator);

  /**
   * cardview_board_const_iterator_free: (skip):
   *
   * Free the iterator.
   *
   * iterator: the iterator.
   * allocator: (value null): 
   */
  void cardview_board_const_iterator_free (CardviewBoardConstIterator *
					   iterator,
					   CardviewAllocator * allocator);

  /**
   * cardview_board_iterator_tell:
   *
   * Get the position of the iterator.
   *
   * iterator: the read/write iterator.
   * hand: which hand we are iterating over.
   * player: which adversary.
   * card: which card.
   */
  void cardview_board_iterator_tell (const CardviewBoardIterator * iterator,
				     CardviewHandType * hand,
				     size_t * player, size_t * card);

  /**
   * cardview_board_const_iterator_tell:
   *
   * Get the position of the iterator.
   *
   * iterator: the read-only iterator.
   * hand: which hand we are iterating over.
   * player: which adversary.
   * card: which card.
   */
  void cardview_board_const_iterator_tell (const CardviewBoardConstIterator *
					   iterator, CardviewHandType * hand,
					   size_t * player, size_t * card);

  /**
   * cardview_board_iterator_get: (skip):
   *
   * Get the value under the iterator.
   *
   * iterator: the iterator.
   * Returns: the associated datum.
   */
  const void *cardview_board_iterator_get (const CardviewBoardIterator *
					   iterator);

  /**
   * cardview_board_iterator_p_get:
   *
   * Get the value under the iterator.
   *
   * iterator: the iterator.
   * Returns: the associated pointer.
   */
  const void *cardview_board_iterator_p_get (const CardviewBoardIterator *
					     iterator);

  /**
   * cardview_board_const_iterator_get: (skip):
   *
   * Get the value under the iterator.
   *
   * iterator: the iterator.
   * Returns: the associated datum.
   */
  const void *cardview_board_const_iterator_get (const
						 CardviewBoardConstIterator *
						 iterator);

  /**
   * cardview_board_const_iterator_p_get:
   *
   * Get the value under the iterator.
   *
   * iterator: the iterator.
   * Returns: the associated pointer.
   */
  const void *cardview_board_const_iterator_p_get (const
						   CardviewBoardConstIterator
						   * iterator);

  /**
   * cardview_board_iterator_set: (skip):
   *
   * Set the value under the iterator.
   *
   * The iterator is declared constant, since modifying the board does
   * not move the iterator.
   *
   * iterator: the iterator.
   * datum: the datum to replace.
   */
  void cardview_board_iterator_set (const CardviewBoardIterator * iterator,
				    const void *datum);

  /**
   * cardview_board_iterator_p_set:
   *
   * Set the pointer under the iterator.
   *
   * The iterator is declared constant, since modifying the board does
   * not move the iterator.
   *
   * iterator: the iterator.
   * datum: the pointer to replace.
   */
  void cardview_board_iterator_p_set (const CardviewBoardIterator * iterator,
				      const void *datum);

  /**
   * cardview_board_iterator_next:
   *
   * Advance the iterator.
   *
   * iterator: the iterator.
   * Returns: (type bool): whether the iterator is still valid after
   * the move.
   */
  int cardview_board_iterator_next (CardviewBoardIterator * iterator);

  /**
   * cardview_board_const_iterator_next:
   *
   * Advance the iterator.
   *
   * iterator: the iterator.
   * Returns: (type bool): whether the iterator is still valid after
   * the move.
   */
  int cardview_board_const_iterator_next (CardviewBoardConstIterator *
					  iterator);

#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif				/* H_CARDVIEW_BOARD_INCLUDED */
