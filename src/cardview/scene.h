/*
 * scene.h
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_CARDVIEW_SCENE_INCLUDED
#define H_CARDVIEW_SCENE_INCLUDED

#include <stddef.h>
#include <cardview/allocator.h>
#include <cardview/model.h>

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */

#ifndef __GTK_DOC_IGNORE__
  struct CardviewScene;
#endif

  /**
   * CardviewScene:
   *
   * A board of cards.
   */
  typedef struct CardviewScene CardviewScene;

  /**
   * cardview_scene_alloc:
   *
   * Create a new scene.
   *
   * allocator: (value NULL): unused in gobject.
   * Returns: (transfer full): a new scene.
   */
  CardviewScene *cardview_scene_alloc (CardviewAllocator * allocator);

  /**
   * cardview_scene_copy:
   *
   * Make a copy of an existing scene.  This operation is costly.
   *
   * existing: an existing scene.
   * dest: an allocated scene which may have different dimensions.
   */
  void cardview_scene_copy (CardviewScene * dest,
			    const CardviewScene * existing);

  /**
   * cardview_scene_dup:
   *
   * Allocate and copy an existing scene.
   *
   * existing: an existing scene.
   * allocator: the allocator.
   * Returns: an allocated copy.
   */
  CardviewScene *cardview_scene_dup (const CardviewScene * existing,
				     CardviewAllocator * allocator);

  /**
   * cardview_scene_free: (skip):
   *
   * Destroy a scene.
   *
   * scene: the scene to destroy.
   * allocator: (value NULL): unused in gobject.
   */
  void cardview_scene_free (CardviewScene * scene,
			    CardviewAllocator * allocator);

  /**
   * cardview_scene_set_model:
   *
   * Set the model associated to the scene.
   *
   * scene: the scene.
   * model: the model.
   */
  void cardview_scene_set_model (CardviewScene * scene,
				 const CardviewModel * model);

  /**
   * cardview_scene_set_view_size:
   *
   * Define the size of the scene.
   *
   * scene: the scene.
   * w: the scene width.
   * h: the scene height.
   */
  void cardview_scene_set_view_size (CardviewScene * scene, float w, float h);

  /**
   * cardview_scene_set_card_size:
   *
   * Define the size of the cards, in scene units.
   *
   * scene: the scene.
   * w: the card width.
   * h: the card height.
   */
  void cardview_scene_set_card_size (CardviewScene * scene, float w, float h);

  /**
   * CardviewRendering:
   *
   * The computed rendering information for a specific card.
   */
  typedef struct
  {

    /**
     * x:
     * The x coordinate in scene units.
     */
    float x;

    /**
     * y:
     * The y coordinate in scene units.
     */
    float y;

    /**
     * z:
     * The z-index.  Low values mean earlier rendering.
     */
    float z;

    /**
     * angle:
     * The rotation angle to apply after the translation.
     */
    float angle;

    /**
     * scale:
     * The scale to apply after the translation.
     */
    float scale;

    /**
     * index:
     * The card index.
     */
    unsigned int index;
  } CardviewRendering;

  /**
   * cardview_scene_position:
   *
   * Compute the position of a card.
   *
   * scene: the scene.
   * hand: which hand.
   * player: which adversary.
   * i_card: which card.
   * rendering: set to the rendering information.
   *
   * Returns: whether the card should be displayed at all.
   */
  int cardview_scene_position (const CardviewScene * scene,
			       CardviewHandType hand, size_t player,
			       size_t i_card, CardviewRendering * rendering);

  /**
   * cardview_scene_positions:
   *
   * scene: the scene.
   * max: the number of cards to draw.
   * rendering: (array length=max): where to store the rendering information.
   * Returns: how many cards should be drawn.
   */
  size_t cardview_scene_positions (const CardviewScene * scene, size_t max,
				   CardviewRendering * rendering);

#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif				/* H_CARDVIEW_SCENE_INCLUDED */
