/*
 * widget.c
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <cardview.h>
#include "cvcairo.h"
#include "scene.h"
#include "controller.h"
#include <cairo.h>

struct _CardviewGtkWidget
{
  GtkDrawingArea parent_instance;

  GdkPixbuf *background_image;
  size_t n_cards;
  GdkPixbuf *cards_image;
  CardviewScene *scene;
  CardviewController *controller;
};

G_DEFINE_TYPE (CardviewGtkWidget, cardview_gtk_widget, GTK_TYPE_DRAWING_AREA)
     static inline float maxi (float a, float b)
{
  if (a > b)
    {
      return a;
    }
  return b;
}

static void
draw_background (cairo_t * cr, GdkPixbuf * bg, guint w_width, guint w_height,
		 guint bg_width, guint bg_height)
{
  const float scale_height = w_height / (float) bg_height;
  const float scale_width = w_width / (float) bg_width;
  const float scale = maxi (scale_height, scale_width);
  const float margin_height = (w_height - scale * bg_height) / 2.0;
  const float margin_width = (w_width - scale * bg_width) / 2.0;
  cairo_matrix_t identity;
  cairo_get_matrix (cr, &identity);
  cairo_translate (cr, margin_width, margin_height);
  cairo_scale (cr, scale, scale);
  gdk_cairo_set_source_pixbuf (cr, bg, 0, 0);
  cairo_paint (cr);
  cairo_set_matrix (cr, &identity);
}

static void
draw_one_card (cairo_t * cr, unsigned int index, void *painter_state)
{
  CardviewGtkWidget *w = (CardviewGtkWidget *) painter_state;
  guint width, height;
  float pos_x, pos_y, pos_w, pos_h;
  width = gdk_pixbuf_get_width (w->cards_image) / w->n_cards;
  height = gdk_pixbuf_get_height (w->cards_image);
  pos_w = width;
  pos_h = height;
  pos_x = -pos_w / 2 - index * pos_w;
  pos_y = -pos_h / 2;
  cairo_save (cr);
  cairo_scale (cr, 1, -1);
  cairo_translate (cr, pos_x, pos_y);
  gdk_cairo_set_source_pixbuf (cr, w->cards_image, 0, 0);
  cairo_rectangle (cr, index * pos_w, 0, pos_w, pos_h);
  cairo_clip (cr);
  cairo_paint (cr);
  cairo_restore (cr);
}

static gboolean
draw (GtkWidget * widget, cairo_t * cr, gpointer data)
{
  CardviewGtkWidget *w = CARDVIEW_GTK_WIDGET (data);
  guint widget_width, widget_height, bg_width, bg_height, card_w, card_h;
  CardviewCairoPainter painter;
  painter.painter_state = (void *) w;
  painter.draw = &draw_one_card;
  if (w != NULL)
    {
      widget_width = gtk_widget_get_allocated_width (widget);
      widget_height = gtk_widget_get_allocated_height (widget);
      card_w = 0;
      card_h = 0;
      if (w->cards_image != NULL)
	{
	  card_w = gdk_pixbuf_get_width (w->cards_image) / w->n_cards;
	  card_h = gdk_pixbuf_get_height (w->cards_image);
	}
      if (w->scene != NULL)
	{
	  cardview_scene_set_view_size (w->scene, widget_width,
					widget_height);
	  cardview_scene_set_card_size (w->scene, card_w, card_h);
	}
      bg_width = gdk_pixbuf_get_width (w->background_image);
      bg_height = gdk_pixbuf_get_height (w->background_image);
      draw_background (cr, w->background_image, widget_width, widget_height,
		       bg_width, bg_height);
      if (w->scene != NULL)
	{
	  cairo_save (cr);
	  cairo_translate (cr, widget_width / 2, widget_height / 2);
	  cairo_scale (cr, 1, -1);
	  cardview_cairo_show (w->scene, &painter, cr, NULL);
	  cairo_restore (cr);
	}
    }
  return FALSE;
}

/* Properties! */
enum
{
  PROP_BACKGROUND_IMAGE = 1,
  PROP_SCENE,
  PROP_CONTROLLER,
  PROP_N_CARDS,
  PROP_CARDS_IMAGE,
  N_PROPERTIES
};

static GParamSpec *class_props[N_PROPERTIES] = { NULL, };

static void
set_property (GObject * object, guint prop_id, const GValue * value,
	      GParamSpec * pspec)
{
  CardviewGtkWidget *widget = CARDVIEW_GTK_WIDGET (object);
  GdkPixbuf *value_pixbuf;
  CardviewScene *value_scene;
  CardviewController *value_controller;
  size_t value_n_cards;
  GdkPixbuf *value_cards_image;
  (void) pspec;
  switch (prop_id)
    {
    case PROP_BACKGROUND_IMAGE:
      value_pixbuf = GDK_PIXBUF (g_value_dup_object (value));
      if (widget->background_image != NULL)
	{
	  g_object_unref (widget->background_image);
	}
      widget->background_image = value_pixbuf;
      break;
    case PROP_SCENE:
      value_scene = (CardviewScene *) g_value_get_pointer (value);
      widget->scene = value_scene;
      break;
    case PROP_CONTROLLER:
      value_controller = (CardviewController *) g_value_get_pointer (value);
      widget->controller = value_controller;
      break;
    case PROP_N_CARDS:
      value_n_cards = g_value_get_ulong (value);
      widget->n_cards = value_n_cards;
      break;
    case PROP_CARDS_IMAGE:
      value_cards_image = GDK_PIXBUF (g_value_dup_object (value));
      if (widget->cards_image != NULL)
	{
	  g_object_unref (widget->cards_image);
	}
      widget->cards_image = value_cards_image;
      break;
    default:
      break;
    }
  gtk_widget_queue_draw (GTK_WIDGET (widget));
}

static void
get_property (GObject * object, guint prop_id, GValue * value,
	      GParamSpec * pspec)
{
  CardviewGtkWidget *widget = CARDVIEW_GTK_WIDGET (object);
  (void) pspec;
  switch (prop_id)
    {
    case PROP_BACKGROUND_IMAGE:
      g_value_set_object (value, G_OBJECT (widget->background_image));
      break;
    case PROP_SCENE:
      g_value_set_pointer (value, (void *) widget->scene);
      break;
    case PROP_CONTROLLER:
      g_value_set_pointer (value, (void *) widget->controller);
      break;
    case PROP_N_CARDS:
      g_value_set_ulong (value, widget->n_cards);
      break;
    case PROP_CARDS_IMAGE:
      g_value_set_object (value, G_OBJECT (widget->cards_image));
      break;
    default:
      break;
    }
}

static void
dispose (GObject * object)
{
  CardviewGtkWidget *widget = CARDVIEW_GTK_WIDGET (object);
  if (widget->background_image != NULL)
    {
      g_object_unref (widget->background_image);
    }
  widget->background_image = NULL;
  widget->scene = NULL;
  widget->controller = NULL;
  G_OBJECT_CLASS (cardview_gtk_widget_parent_class)->dispose (object);
}

static void
finalize (GObject * object)
{
  G_OBJECT_CLASS (cardview_gtk_widget_parent_class)->finalize (object);
}

void
cardview_gtk_widget_forward (CardviewGtkWidget * widget)
{
  cardview_controller_point_forward (widget->controller);
  gtk_widget_queue_draw (GTK_WIDGET (widget));
}

void
cardview_gtk_widget_backward (CardviewGtkWidget * widget)
{
  cardview_controller_point_backward (widget->controller);
  gtk_widget_queue_draw (GTK_WIDGET (widget));
}

void
cardview_gtk_widget_next (CardviewGtkWidget * widget)
{
  cardview_controller_point_next (widget->controller);
  gtk_widget_queue_draw (GTK_WIDGET (widget));
}

void
cardview_gtk_widget_previous (CardviewGtkWidget * widget)
{
  cardview_controller_point_previous (widget->controller);
  gtk_widget_queue_draw (GTK_WIDGET (widget));
}

void
cardview_gtk_widget_toggle (CardviewGtkWidget * widget)
{
  cardview_controller_toggle_selected (widget->controller);
  gtk_widget_queue_draw (GTK_WIDGET (widget));
}

static void
cardview_gtk_widget_init (CardviewGtkWidget * widget)
{
  GObject *obj = G_OBJECT (widget);
  g_signal_connect (obj, "draw", G_CALLBACK (draw), widget);
}

static void
cardview_gtk_widget_class_init (CardviewGtkWidgetClass * klass)
{
  GObjectClass *ok = G_OBJECT_CLASS (klass);
  ok->set_property = set_property;
  ok->get_property = get_property;
  ok->dispose = dispose;
  ok->finalize = finalize;
  class_props[PROP_BACKGROUND_IMAGE] =
    g_param_spec_object ("background_image", "Background",
			 "the background image (as a GdkPixbuf) to display.  "
			 "Cardview provides at least "
			 "the green.png background.",
			 GDK_TYPE_PIXBUF, G_PARAM_READWRITE);
  class_props[PROP_SCENE] =
    g_param_spec_pointer ("scene", "Card scene",
			  "the scene of the cards (tracks what card is where).",
			  G_PARAM_READWRITE);
  class_props[PROP_CONTROLLER] =
    g_param_spec_pointer ("controller", "Card controller",
			  "the controller of the view (tracks the point and manages selection).",
			  G_PARAM_READWRITE);
  class_props[PROP_N_CARDS] =
    g_param_spec_ulong ("n_cards", "Number of cards",
			"the number of cards in the cards image.", 1,
			G_MAXULONG, 1, G_PARAM_READWRITE);
  class_props[PROP_CARDS_IMAGE] =
    g_param_spec_object ("cards_image", "Cards image",
			 "the image of n_cards cards, side by side.",
			 GDK_TYPE_PIXBUF, G_PARAM_READWRITE);
  g_object_class_install_properties (ok, N_PROPERTIES, class_props);
}

CardviewGtkWidget *
cardview_gtk_widget_new ()
{
  return g_object_new (CARDVIEW_GTK_TYPE_WIDGET, NULL);
}
