/*
 * scene.c
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include "scene.h"
#include <stdlib.h>
#include <stdio.h>
#ifdef HAVE_MATH_H
#include <math.h>
#endif /* HAVE_MATH_H */
#ifdef HAVE_TGMATH_H
#include <tgmath.h>
#endif /* HAVE_TGMATH_H */
#include <string.h>

#define ALPHA 0.25

#ifndef __GTK_DOC_IGNORE__
struct CardviewScene
{
  const CardviewModel *model;
  float view_w;
  float view_h;
  float card_w;
  float card_h;

  /*
   * The radius of the main player's hand, if all cards are selected.
   */
  float r;

  /*
   * The angle of the main player's hand.
   */
  float main_angle;

  /*
   * The margin of the foreground hand.
   */
  float foreground_margin;

  /*
   * The angle for each adversary.
   */
  float player_angle;

  /*
   * The scale of a player's card.
   */
  float main_scale;

  /*
   * The scale for an adversary's card.
   */
  float adv_scale;

  /*
   * The scale of a card in the trick.
   */
  float trick_scale;

  /*
   * The scale of a card in the foreground.
   */
  float foreground_scale;
};
#endif

CardviewScene *
cardview_scene_alloc (CardviewAllocator * allocator)
{
  CardviewScene *ret = NULL;
  ret = cardview_alloc (sizeof (CardviewScene), allocator);
  if (ret != NULL)
    {
      ret->model = NULL;
      ret->view_w = 0;
      ret->view_h = 0;
      ret->card_w = 0;
      ret->card_h = 0;
      ret->r = 0;
      ret->main_angle = 0;
      ret->foreground_margin = 0;
      ret->player_angle = 0;
      ret->main_scale = 0;
      ret->adv_scale = 0;
      ret->trick_scale = 0;
      ret->foreground_scale = 0;
    }
  return ret;
}

void
cardview_scene_copy (CardviewScene * dest, const CardviewScene * existing)
{
  memcpy (dest, existing, sizeof (CardviewScene));
}

void
cardview_scene_free (CardviewScene * scene, CardviewAllocator * allocator)
{
  cardview_free (scene, allocator);
}

#define MINI(a, b) ((a) < (b) ? (a) : (b))

#define MAXI(a, b) ((a) > (b) ? (a) : (b))

static void
update (CardviewScene * scene)
{
  const float alphah = ALPHA * MINI (scene->view_h, scene->view_w);
  const float R =
    (scene->view_w * scene->view_w + alphah * alphah) / (4 * alphah);
  const float main_extension = atan2f (scene->view_w, 2 * R - alphah);
  const float card_ratio = scene->card_w / scene->card_h;
  const float main_card_h = MINI (scene->view_h / 2, scene->view_w / 2);
  const float main_card_w = card_ratio * main_card_h;
  const float main_margin = atan2f (main_card_w / 2, R);
  const float main_angle = main_extension - main_margin;
  const float foreground_card_h_wide = scene->view_h / 2;
  const float foreground_card_w_tall = scene->view_w / 2;
  const float foreground_card_w_wide = foreground_card_h_wide * card_ratio;
  const float foreground_card_w =
    MINI (foreground_card_w_tall, foreground_card_w_wide);
  const float foreground_margin = foreground_card_w / 2;
  const size_t n_players = cardview_model_n_players (scene->model);
  const float player_angle = M_PI / n_players;
  const float main_scale = main_card_w / scene->card_w;
  const float adv_card_w = foreground_card_w / 6;
  const float adv_scale = adv_card_w / scene->card_w;
  const float trick_card_w = foreground_card_w / 4;
  const float trick_scale = trick_card_w / scene->card_w;
  const float foreground_scale = foreground_card_w / scene->card_w;
  scene->r = R;
  scene->main_angle = main_angle;
  scene->foreground_margin = foreground_margin;
  scene->player_angle = player_angle;
  scene->main_scale = main_scale;
  scene->adv_scale = adv_scale;
  scene->trick_scale = trick_scale;
  scene->foreground_scale = foreground_scale;
}

void
cardview_scene_set_model (CardviewScene * scene, const CardviewModel * model)
{
  scene->model = model;
  if (scene->model != NULL)
    {
      update (scene);
    }
}

void
cardview_scene_set_view_size (CardviewScene * scene, float w, float h)
{
  scene->view_w = w;
  scene->view_h = h;
  if (scene->model != NULL)
    {
      update (scene);
    }
}

void
cardview_scene_set_card_size (CardviewScene * scene, float w, float h)
{
  scene->card_w = w;
  scene->card_h = h;
  if (scene->model != NULL)
    {
      update (scene);
    }
}

static void
position_main (const CardviewScene * scene, size_t i, size_t n, int selected,
	       int pointed, float *x, float *y, float *z, float *angle,
	       float *scale)
{
  const float pc = (i + 1.0) / (n + 1.0);
  const float theta = -(2 * pc - 1) * scene->main_angle;
  const float sc = scene->main_scale;
  const float r_selected = scene->r - (scene->view_h / 4);
  const float r_not_selected = r_selected - (scene->view_h / 8);
  const float r = (selected ? r_selected : r_not_selected);
  const float x_center = -r * sinf (theta);
  const float y_center = r * cosf (theta) - scene->r;
  *angle = theta;
  *x = x_center;
  *y = y_center;
  *z = 2 + pc;
  *scale = sc;
  if (pointed)
    {
      *z = 3;
    }
}

static void
position_adv (const CardviewScene * scene, size_t player, size_t n_players,
	      size_t i, size_t n, int selected, int pointed, float *x,
	      float *y, float *z, float *angle, float *scale)
{
  const float player_pc = (player + 1.0) / (n_players + 1.0);
  const float player_angle = M_PI * (1 - player_pc);
  const float x_player = scene->view_w / 2 * cos (player_angle);
  const float y_player = scene->view_h / 2 * sin (player_angle);
  const float pc = (i + 1.0) / (n + 1.0);
  const float card_angle = M_PI * pc;
  const float absolute_angle = player_angle - card_angle;
  const float card_h = scene->adv_scale * scene->card_h;
  const float dx = (card_h / 2) * sinf (absolute_angle);
  const float dy = -(card_h / 2) * cosf (absolute_angle);
  *x = x_player + dx;
  *y = y_player + dy;
  *z = player_pc * pc - 1;
  *angle = absolute_angle;
  *scale = scene->adv_scale;
  if (selected)
    {
      *scale *= 1.25;
    }
  if (pointed)
    {
      *z = 3;
    }
}

static void
position_trick (const CardviewScene * scene, size_t i, size_t n, int selected,
		int pointed, float *x, float *y, float *z, float *angle,
		float *scale)
{
  const float y_center = scene->view_h / 4;
  const float adv_pc = (i + 1.0) / n;
  const float angle_adv = M_PI * (1 - adv_pc);
  const float angle_main = -M_PI / 2;
  const float theta = (i + 1 == n ? angle_main : angle_adv);
  const float radius_w = scene->view_w / 6;
  const float card_h = scene->trick_scale * scene->card_h;
  const float radius_h = card_h;
  *x = radius_w * cos (theta);
  *y = radius_h * sin (theta) + y_center;
  *z = adv_pc;
  *angle = 0;
  *scale = scene->trick_scale;
  if (selected)
    {
      *scale *= 1.25;
    }
  if (pointed)
    {
      *z = 3;
    }
}

static void
position_foreground (const CardviewScene * scene, size_t i, size_t n,
		     int selected, int pointed, float *x, float *y, float *z,
		     float *angle, float *scale)
{
  const float margin = scene->foreground_margin;
  const float pc = (i + 1.0) / (n + 1.0);
  *x = (2 * pc - 1) * (scene->view_w - margin);
  *y = 0;
  *z = pc + 1;
  *angle = 0;
  *scale = scene->foreground_scale;
  if (selected)
    {
      *scale *= 1.25;
    }
  if (pointed)
    {
      *z = 3;
    }
}

int
cardview_scene_position (const CardviewScene * scene, CardviewHandType hand,
			 size_t player, size_t i_card,
			 CardviewRendering * rendering)
{
  int selected, pointed;
  CardviewHandType pointed_hand;
  size_t pointed_player, i_pointed;
  rendering->index =
    cardview_model_get_index (scene->model, hand, player, i_card);
  selected = cardview_model_get_selected (scene->model, hand, player, i_card);
  pointed =
    cardview_model_get_pointed (scene->model, &pointed_hand, &pointed_player,
				&i_pointed);
  pointed = pointed && (pointed_hand == hand
			&& (hand != CARDVIEW_HAND_ADVERSARY
			    || player == pointed_player)
			&& i_pointed == i_card);
  switch (hand)
    {
    case CARDVIEW_HAND_MAIN:
      position_main (scene, i_card,
		     cardview_model_size (scene->model, hand, player),
		     selected, pointed, &(rendering->x), &(rendering->y),
		     &(rendering->z), &(rendering->angle),
		     &(rendering->scale));
      break;
    case CARDVIEW_HAND_ADVERSARY:
      position_adv (scene, player, cardview_model_n_players (scene->model),
		    i_card, cardview_model_size (scene->model, hand, player),
		    selected, pointed, &(rendering->x), &(rendering->y),
		    &(rendering->z), &(rendering->angle),
		    &(rendering->scale));
      break;
    case CARDVIEW_HAND_TRICK:
      position_trick (scene, i_card,
		      cardview_model_size (scene->model, hand, player),
		      selected, pointed, &(rendering->x), &(rendering->y),
		      &(rendering->z), &(rendering->angle),
		      &(rendering->scale));
      break;
    case CARDVIEW_HAND_FOREGROUND:
      position_foreground (scene, i_card,
			   cardview_model_size (scene->model, hand, player),
			   selected, pointed, &(rendering->x),
			   &(rendering->y), &(rendering->z),
			   &(rendering->angle), &(rendering->scale));
      break;
    }
  return 1;
}

static int
compare (const void *x, const void *y)
{
  const CardviewRendering *rx = (const CardviewRendering *) x;
  const CardviewRendering *ry = (const CardviewRendering *) y;
  if (rx->z > ry->z)
    {
      return 1;
    }
  else if (rx->z < rx->z)
    {
      return -1;
    }
  return 0;
}

static void
sort_ri (size_t n, CardviewRendering * cards)
{
  qsort (cards, n, sizeof (CardviewRendering), compare);
}

static void
add_hand (const CardviewScene * scene, CardviewHandType hand, size_t player,
	  size_t * where, size_t * remaining, CardviewRendering * cards)
{
  size_t n = cardview_model_size (scene->model, hand, player);
  size_t i = 0;
  unsigned int ok = 0;
  for (i = 0; i < n; i++)
    {
      CardviewRendering out;
      ok = ! !cardview_scene_position (scene, hand, player, i, &out);
      if (*remaining >= ok)
	{
	  memcpy (&(cards[*where]), &out, sizeof (CardviewRendering));
	  *remaining -= ok;
	}
      *where += ok;
    }
}

static void
add_all (const CardviewScene * scene, size_t * where, size_t * remaining,
	 CardviewRendering * cards)
{
  size_t n = cardview_model_n_players (scene->model);
  size_t p;
  add_hand (scene, CARDVIEW_HAND_MAIN, 0, where, remaining, cards);
  for (p = 0; p < n; p++)
    {
      add_hand (scene, CARDVIEW_HAND_ADVERSARY, p, where, remaining, cards);
    }
  add_hand (scene, CARDVIEW_HAND_TRICK, 0, where, remaining, cards);
  add_hand (scene, CARDVIEW_HAND_FOREGROUND, 0, where, remaining, cards);
}

size_t
cardview_scene_positions (const CardviewScene * scene, size_t max,
			  CardviewRendering * cards)
{
  size_t where = 0, remaining = max;
  add_all (scene, &where, &remaining, cards);
  if (where <= max)
    {
      sort_ri (where, cards);
    }
  return where;
}
