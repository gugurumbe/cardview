/*
 * demo_application_window.c
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <cardview.h>
#include <gtk/gtk.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "demo_application.h"
#include "demo_application_window.h"

struct _CardviewDemoApplicationWindow
{
  GtkApplicationWindow parent;

  CardviewGtkWidget *view;
};

G_DEFINE_TYPE (CardviewDemoApplicationWindow, cardview_demo_application_window, GTK_TYPE_APPLICATION_WINDOW);

static void
on_left_clicked (CardviewGtkWidget *view)
{
  fprintf (stderr, "%s:%d: on_left_clicked!\n", __FILE__, __LINE__);
  cardview_gtk_widget_backward (view);
}

static void
on_right_clicked (CardviewGtkWidget *view)
{
  fprintf (stderr, "%s:%d: on_right_clicked!\n", __FILE__, __LINE__);
  cardview_gtk_widget_forward (view);
}

static void
on_up_clicked (CardviewGtkWidget *view)
{
  fprintf (stderr, "%s:%d: on_up_clicked!\n", __FILE__, __LINE__);
  cardview_gtk_widget_previous (view);
}

static void
on_down_clicked (CardviewGtkWidget *view)
{
  fprintf (stderr, "%s:%d: on_down_clicked!\n", __FILE__, __LINE__);
  cardview_gtk_widget_next (view);
}

static void
on_toggle_clicked (CardviewGtkWidget *view)
{
  fprintf (stderr, "%s:%d: on_toggle_clicked!\n", __FILE__, __LINE__);
  cardview_gtk_widget_toggle (view);
}

static void
cardview_demo_application_window_init (CardviewDemoApplicationWindow *window)
{
  gtk_widget_init_template (GTK_WIDGET (window));
}

static GBytes *
read_all (const char *file)
{
  GError *error;
  gchar *dest;
  gsize length;  
  if (!g_file_get_contents (file, &dest, &length, &error))
    {
      fprintf (stderr, "Could not load the user interface: %s\n", error->message);
      exit (1);
    }
  return g_bytes_new_take (dest, length);
}

#define HANDLER(name)				\
  gtk_widget_class_bind_template_callback_full (GTK_WIDGET_CLASS (klass), #name, G_CALLBACK (name))

static void
cardview_demo_application_window_class_init (CardviewDemoApplicationWindowClass *klass)
{
  g_autoptr (GBytes) bytes = read_all (UIDIR "demo.glade");
  gtk_widget_class_set_template(GTK_WIDGET_CLASS (klass), bytes);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (klass), CardviewDemoApplicationWindow, view);
  HANDLER (on_left_clicked);
  HANDLER (on_right_clicked);
  HANDLER (on_up_clicked);
  HANDLER (on_down_clicked);
  HANDLER (on_toggle_clicked);
}

CardviewDemoApplicationWindow *
cardview_demo_application_window_new (CardviewDemoApplication *app)
{
  CardviewDemoApplicationWindow *window = g_object_new (CARDVIEW_TYPE_DEMO_APPLICATION_WINDOW, "application", app, NULL);
  GValue val_scene = G_VALUE_INIT;
  GValue val_controller = G_VALUE_INIT;
  g_value_init (&val_scene, G_TYPE_POINTER);
  g_value_set_pointer (&val_scene, (void *) cardview_demo_application_get_scene (app));
  g_object_set_property (G_OBJECT (window->view), "scene", &val_scene);
  g_value_unset (&val_scene);
  g_value_init (&val_controller, G_TYPE_POINTER);
  g_value_set_pointer (&val_controller, (void *) cardview_demo_application_get_controller (app));
  g_object_set_property (G_OBJECT (window->view), "controller", &val_controller);
  g_value_unset (&val_controller);
  return window;
}

void
cardview_demo_application_window_open (CardviewDemoApplicationWindow *window, GFile *file)
{
  (void) window;
  (void) file;
}
