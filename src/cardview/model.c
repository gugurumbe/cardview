/*
 * model.c
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include "model.h"
#include "board.h"
#include <stdio.h>

typedef struct
{
  unsigned int index;
  int selected;
} InnerType;

#ifndef __GTK_DOC_IGNORE__
struct CardviewModel
{
  CardviewBoard *m;
  int has_pointed;
  CardviewHandType pointed_hand;
  size_t pointed_player;
  size_t pointed_card;
};

struct CardviewModelIterator
{
  CardviewBoardIterator *it;
};
#endif

CardviewModel *
cardview_model_alloc (size_t n_other_players, size_t max,
		      CardviewAllocator * allocator)
{
  CardviewModel *ret = NULL;
  ret = cardview_alloc (sizeof (CardviewModel), allocator);
  if (ret != NULL)
    {
      ret->has_pointed = 0;
      ret->pointed_hand = CARDVIEW_HAND_MAIN;
      ret->pointed_player = 0;
      ret->pointed_card = 0;
      ret->m =
	cardview_board_alloc (n_other_players, max, sizeof (InnerType),
			      allocator);
      if (ret->m == NULL)
	{
	  cardview_free (ret, allocator);
	  ret = NULL;
	}
    }
  return ret;
}

void
cardview_model_copy (CardviewModel * dest, const CardviewModel * existing)
{
  cardview_board_copy (dest->m, existing->m);
  dest->has_pointed = existing->has_pointed;
  dest->pointed_hand = existing->pointed_hand;
  dest->pointed_player = existing->pointed_player;
  dest->pointed_card = existing->pointed_card;
}

CardviewModel *
cardview_model_dup (const CardviewModel * model,
		    CardviewAllocator * allocator)
{
  CardviewModel *ret = cardview_model_alloc (cardview_model_n_players (model),
					     cardview_model_max (model),
					     allocator);
  cardview_model_copy (ret, model);
  return ret;
}

void
cardview_model_free (CardviewModel * model, CardviewAllocator * allocator)
{
  cardview_board_free (model->m, allocator);
  cardview_free (model, allocator);
}

size_t
cardview_model_n_players (const CardviewModel * model)
{
  return cardview_board_n_players (model->m);
}

size_t
cardview_model_max (const CardviewModel * model)
{
  return cardview_board_max (model->m);
}

size_t
cardview_model_size (const CardviewModel * model, CardviewHandType hand,
		     size_t player)
{
  return cardview_board_size (model->m, hand, player);
}

unsigned int
cardview_model_get_index (const CardviewModel * model, CardviewHandType hand,
			  size_t player, size_t i)
{
  const void *raw = cardview_board_get (model->m, hand, player, i);
  InnerType *typed = (InnerType *) raw;
  return typed->index;
}

int
cardview_model_get_selected (const CardviewModel * model,
			     CardviewHandType hand, size_t player, size_t i)
{
  const void *raw = cardview_board_get (model->m, hand, player, i);
  InnerType *typed = (InnerType *) raw;
  return typed->selected;
}

int
cardview_model_get_pointed (const CardviewModel * model,
			    CardviewHandType * hand, size_t * player,
			    size_t * i)
{
  *hand = model->pointed_hand;
  *player = model->pointed_player;
  *i = model->pointed_card;
  return model->has_pointed;
}

void
cardview_model_set_index (CardviewModel * model, CardviewHandType hand,
			  size_t player, size_t i, unsigned int index)
{
  const void *raw = cardview_board_get (model->m, hand, player, i);
  InnerType *typed = (InnerType *) raw;
  typed->index = index;
}

void
cardview_model_set_selected (CardviewModel * model, CardviewHandType hand,
			     size_t player, size_t i, int selected)
{
  const void *raw = cardview_board_get (model->m, hand, player, i);
  InnerType *typed = (InnerType *) raw;
  typed->selected = selected;
}

void
cardview_model_set_pointed (CardviewModel * model, CardviewHandType hand,
			    size_t player, size_t i)
{
  model->pointed_hand = hand;
  model->pointed_player = player;
  model->pointed_card = i;
  model->has_pointed = 1;
}

void
cardview_model_unset_pointed (CardviewModel * model)
{
  model->has_pointed = 0;
}

void
cardview_model_resize (CardviewModel * model, CardviewHandType hand,
		       size_t player, size_t new_size)
{
  cardview_board_resize (model->m, hand, player, new_size);
}

CardviewModelIterator *
cardview_model_iterator (const CardviewModel * model,
			 CardviewAllocator * allocator)
{
  CardviewModelIterator *ret =
    cardview_alloc (sizeof (CardviewModelIterator), allocator);
  if (ret != NULL)
    {
      ret->it = cardview_board_iterator (model->m, allocator);
      if (ret->it == NULL)
	{
	  cardview_free (ret, allocator);
	  ret = NULL;
	}
    }
  return ret;
}

CardviewModelIterator *
cardview_model_iterator_dup (const CardviewModelIterator * iterator,
			     CardviewAllocator * allocator)
{
  CardviewModelIterator *ret =
    cardview_alloc (sizeof (CardviewModelIterator), allocator);
  if (ret != NULL)
    {
      ret->it = cardview_board_iterator_dup (iterator->it, allocator);
      if (ret == NULL)
	{
	  cardview_free (ret, allocator);
	  ret = NULL;
	}
    }
  return ret;
}

void
cardview_model_iterator_free (CardviewModelIterator * iterator,
			      CardviewAllocator * allocator)
{
  cardview_board_iterator_free (iterator->it, allocator);
  cardview_free (iterator, allocator);
}

void
cardview_model_iterator_tell (const CardviewModelIterator * iterator,
			      CardviewHandType * hand, size_t * player,
			      size_t * i)
{
  cardview_board_iterator_tell (iterator->it, hand, player, i);
}

unsigned int
cardview_model_iterator_get_index (const CardviewModelIterator * iterator)
{
  const void *raw = cardview_board_iterator_get (iterator->it);
  const InnerType *data = (InnerType *) raw;
  return data->index;
}

int
cardview_model_iterator_get_selected (const CardviewModelIterator * iterator)
{
  const void *raw = cardview_board_iterator_get (iterator->it);
  const InnerType *data = (InnerType *) raw;
  return data->selected;
}

int
cardview_model_iterator_next (CardviewModelIterator * iterator)
{
  return cardview_board_iterator_next (iterator->it);
}
