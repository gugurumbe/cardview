/*
 * cvcairo.h: The cairo function to draw the card view
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_CARDVIEW_CAIRO_INCLUDED
#define H_CARDVIEW_CAIRO_INCLUDED

#include <cairo.h>
#include <cardview/allocator.h>
#include <cardview/scene.h>

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */

  /**
   * CardviewCairoPointer:
   * A function to draw a card in a prepared cairo context.
   */
  typedef struct
  {
    /**
     * draw:
     *
     * The drawing routine itself.
     *
     * The implementation should draw a card, centered at (0, 0).
     *
     * @cr: the Cairo context to use.
     * @index: the index of the card to draw.
     * @painter_state: the state of the painter, so that this function
     * can be reentrant.
     */
    void (*draw) (cairo_t * cr, unsigned int index, void *painter_state);

    /**
     * painter_state:
     *
     * The closure environment, passed as the last parameter of
     * draw().
     */
    void *painter_state;
  } CardviewCairoPainter;

  /**
   * cardview_cairo_show:
   * Draw a board to a cairo context.
   *
   * scene: the scene.
   * painter: the callback function to paint each individual card.
   * cr: the cairo context.
   * allocator: (value NULL): allocate internal work arrays.
   */
  void cardview_cairo_show (const CardviewScene * scene,
			    CardviewCairoPainter * painter,
			    cairo_t * cr, CardviewAllocator * allocator);

#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif				/* H_CARDVIEW_CAIRO_INCLUDED */
