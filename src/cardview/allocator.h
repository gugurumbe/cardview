/*
 * allocator.h
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_CARDVIEW_ALLOCATOR_INCLUDED
#define H_CARDVIEW_ALLOCATOR_INCLUDED

#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */

  /**
   * CardviewAllocator: (skip):
   *
   * An allocator not to depend on malloc.
   */
  typedef struct
  {
    /**
     * user_data:
     * The environment for the closure.
     */
    void *user_data;

    /**
     * alloc:
     * Allocate @size bytes.
     * user_data: the closure environment.
     * size: the number of bytes to allocate.
     * Returns allocated data, or NULL if the allocation failed.
     */
    void *(*alloc) (void *user_data, size_t size);

    /**
     * free:
     * Free @memory.
     * user_data: the closure environment.
     * memory: the memory to free.
     */
    void (*free) (void *user_data, void *memory);
  } CardviewAllocator;

  /**
   * cardview_alloc: (skip):
   *
   * Allocate memory.
   *
   * size: the number of bytes to allocate.
   * allocator: (value NULL): the allocator to use, or NULL to use the
   * heap.
   * Returns: newly allocated data.
   */
  void *cardview_alloc (size_t size, CardviewAllocator * allocator);

  /**
   * cardview_free: (skip):
   *
   * Free memory.
   *
   * mem: the memory area to de-allocate.
   * allocator: (value NULL): the allocator to use, or NULL to use the
   * heap.
   */
  void cardview_free (void *mem, CardviewAllocator * allocator);

#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif				/* H_CARDVIEW_ALLOCATOR_INCLUDED */
