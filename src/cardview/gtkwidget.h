/*
 * gtkwidget.h
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_CARDVIEW_GTKWIDGET_INCLUDED
#define H_CARDVIEW_GTKWIDGET_INCLUDED

#include <gtk/gtk.h>

G_BEGIN_DECLS
#define CARDVIEW_GTK_TYPE_WIDGET (cardview_gtk_widget_get_type ())
G_DECLARE_FINAL_TYPE (CardviewGtkWidget, cardview_gtk_widget, CARDVIEW_GTK,
		      WIDGET, GtkDrawingArea)
     CardviewGtkWidget *cardview_gtk_widget_new ();

/**
 * cardview_gtk_widget_forward:
 *
 * Point to the next card.
 *
 * widget: the widget.
 */
     void cardview_gtk_widget_forward (CardviewGtkWidget * widget);

/**
 * cardview_gtk_widget_backward:
 *
 * Point to the previous card.
 *
 * widget: the widget.
 */
     void cardview_gtk_widget_backward (CardviewGtkWidget * widget);

/**
 * cardview_gtk_widget_next:
 *
 * Point to the next hand.
 *
 * widget: the widget.
 */
     void cardview_gtk_widget_next (CardviewGtkWidget * widget);

/**
 * cardview_gtk_widget_previous:
 *
 * Point to the previous hand.
 *
 * widget: the widget.
 */
     void cardview_gtk_widget_previous (CardviewGtkWidget * widget);

/**
 * cardview_gtk_widget_toggle:
 *
 * Toggle the selection under the point.
 *
 * widget: the widget.
 */
     void cardview_gtk_widget_toggle (CardviewGtkWidget * widget);

G_END_DECLS
#endif /* H_CARDVIEW_GTKWIDGET_INCLUDED */
