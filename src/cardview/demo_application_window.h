/*
 * demo_application_window.h
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_CARDVIEW_DEMO_APPLICATION_WINDOW_INCLUDED
#define H_CARDVIEW_DEMO_APPLICATION_WINDOW_INCLUDED

#include <gtk/gtk.h>
#include <cardview.h>

G_BEGIN_DECLS
#define CARDVIEW_TYPE_DEMO_APPLICATION_WINDOW (cardview_demo_application_window_get_type ())
G_DECLARE_FINAL_TYPE (CardviewDemoApplicationWindow, cardview_demo_application_window, CARDVIEW,
		      DEMO_APPLICATION_WINDOW, GtkApplicationWindow)
     CardviewDemoApplicationWindow *cardview_demo_application_window_new (CardviewDemoApplication *app);

void cardview_demo_application_window_open (CardviewDemoApplicationWindow *window, GFile *file);

G_END_DECLS
#endif /* H_CARDVIEW_DEMO_APPLICATION_INCLUDED */
