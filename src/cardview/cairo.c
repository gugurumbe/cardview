/*
 * cairo.c: Draw with cairo
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include "cvcairo.h"
#include "scene.h"
#include <stdlib.h>
#include <stdio.h>		/* to abort the program */

#define _(s) s

void
cardview_cairo_show (const CardviewScene * scene,
		     CardviewCairoPainter * painter, cairo_t * cr,
		     CardviewAllocator * allocator)
{
  size_t needed;
  size_t allocated = 0;
  CardviewRendering *buffer;
  needed = cardview_scene_positions (scene, 0, NULL);
  allocated = needed;
  buffer = cardview_alloc (allocated * sizeof (CardviewRendering), allocator);
  if (buffer != NULL)
    {
      size_t usable;
      size_t i;
      needed = cardview_scene_positions (scene, allocated, buffer);
      usable = needed;
      if (usable > allocated)
	{
	  usable = allocated;
	}
      for (i = 0; i < usable; ++i)
	{
	  cairo_save (cr);
	  cairo_translate (cr, buffer[i].x, buffer[i].y);
	  cairo_rotate (cr, buffer[i].angle);
	  cairo_scale (cr, buffer[i].scale, buffer[i].scale);
	  painter->draw (cr, buffer[i].index, painter->painter_state);
	  cairo_restore (cr);
	}
      cardview_free (buffer, allocator);
    }
}
