/*
 * allocator.c
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include "allocator.h"

#define _(s) s

void *
default_alloc (size_t size)
{
  void *ret = malloc (size);
  if (ret == NULL)
    {
      fprintf (stderr, _("Dynamic memory exhauted, exiting.\n"));
      exit (1);
    }
  return ret;
}

void *
cardview_alloc (size_t size, CardviewAllocator * allocator)
{
  if (allocator == NULL)
    {
      return default_alloc (size);
    }
  return allocator->alloc (allocator->user_data, size);
}

void
cardview_free (void *mem, CardviewAllocator * allocator)
{
  if (allocator == NULL)
    {
      free (mem);
    }
  else
    {
      allocator->free (allocator->user_data, mem);
    }
}
