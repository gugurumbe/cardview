/*
 * controller.c
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include "controller.h"
#include "board.h"
#include <stdio.h>
#include <assert.h>

typedef struct
{
  int selectable;
} InnerType;

typedef struct
{
  size_t i_pointed;
} PointedCard;

#ifndef __GTK_DOC_IGNORE__
struct CardviewController
{
  CardviewModel *model;
  CardviewBoard *data;
  CardviewBoard *pointed;
};
#endif

CardviewController *
cardview_controller_alloc (size_t n_players, size_t n_cards,
			   CardviewAllocator * allocator)
{
  CardviewController *ret =
    cardview_alloc (sizeof (CardviewController), allocator);
  if (ret != NULL)
    {
      ret->model = NULL;
      ret->data =
	cardview_board_alloc (n_players, n_cards, sizeof (InnerType),
			      allocator);
      if (ret->data == NULL)
	{
	  cardview_free (ret, allocator);
	  ret = NULL;
	}
      else
	{
	  /* We only want one pointed pos per hand */
	  ret->pointed =
	    cardview_board_alloc (n_players, 1, sizeof (PointedCard),
				  allocator);
	  if (ret->pointed == NULL)
	    {
	      cardview_board_free (ret->data, allocator);
	      cardview_free (ret, allocator);
	      ret = NULL;
	    }
	  else
	    {
	      size_t player, card;
#define SETUP(hand, player)\
	      {\
		PointedCard pt;\
		pt.i_pointed = 0;\
		cardview_board_resize (ret->data, CARDVIEW_HAND_##hand, player, n_cards);\
		for (card = 0; card < n_cards; ++card)\
		  {\
		  InnerType it; \
		  it.selectable = 0;\
		  cardview_board_set (ret->data, CARDVIEW_HAND_##hand, player, card, &it);\
		  }\
		cardview_board_resize (ret->pointed, CARDVIEW_HAND_##hand, player, 1);\
		cardview_board_set (ret->pointed, CARDVIEW_HAND_##hand, player, card, &pt);\
	    }
	      SETUP (MAIN, 0);
	      for (player = 0; player < n_players; ++player)
		{
		  SETUP (ADVERSARY, player);
		}
	      SETUP (TRICK, 0);
	      SETUP (FOREGROUND, 0);
	    }
	}
    }
  return ret;
}

void
cardview_controller_free (CardviewController * controller,
			  CardviewAllocator * allocator)
{
  cardview_board_free (controller->pointed, allocator);
  cardview_board_free (controller->data, allocator);
  cardview_free (controller, allocator);
}

void
cardview_controller_copy (CardviewController * dest,
			  const CardviewController * controller)
{
  dest->model = controller->model;
  cardview_board_copy (dest->data, controller->data);
  cardview_board_copy (dest->pointed, controller->pointed);
}

CardviewController *
cardview_controller_dup (const CardviewController * controller,
			 CardviewAllocator * allocator)
{
  CardviewController *ret =
    cardview_controller_alloc (cardview_controller_n_players (controller),
			       cardview_controller_max (controller),
			       allocator);
  cardview_controller_copy (ret, controller);
  return ret;
}

size_t
cardview_controller_n_players (const CardviewController * controller)
{
  return cardview_board_n_players (controller->data);
}

size_t
cardview_controller_max (const CardviewController * controller)
{
  return cardview_board_max (controller->data);
}

void
cardview_controller_set_model (CardviewController * controller,
			       const CardviewModel * model)
{
  controller->model = (CardviewModel *) model;
}

void
cardview_controller_set_selectable (CardviewController * controller,
				    CardviewHandType hand, size_t player,
				    size_t i, int selectable)
{
  InnerType *ptr = NULL;
  ptr = (InnerType *) cardview_board_get (controller->data, hand, player, i);
  ptr->selectable = selectable;
  if (!selectable)
    {
      cardview_model_set_selected (controller->model, hand, player, i, 0);
    }
}

int
cardview_controller_get_pointed (const CardviewController * controller,
				 CardviewHandType * hand, size_t * player,
				 size_t * i)
{
  return cardview_model_get_pointed (controller->model, hand, player, i);
}

void
cardview_controller_set_pointed (CardviewController * controller,
				 CardviewHandType hand, size_t player,
				 size_t i)
{
  PointedCard *pt;
  pt =
    (PointedCard *) cardview_board_get (controller->pointed, hand, player, 0);
  pt->i_pointed = i;
  cardview_model_set_pointed (controller->model, hand, player, i);
}

void
cardview_controller_unset_pointed (CardviewController * controller)
{
  cardview_model_unset_pointed (controller->model);
}

void
cardview_controller_point_forward (CardviewController * controller)
{
  size_t hand_size;
  PointedCard *pt;
  CardviewHandType pointed_hand;
  size_t pointed_player;
  size_t unused;
  cardview_model_get_pointed (controller->model, &pointed_hand,
			      &pointed_player, &unused);
  hand_size =
    cardview_model_size (controller->model, pointed_hand, pointed_player);
  pt =
    (PointedCard *) cardview_board_get (controller->pointed, pointed_hand,
					pointed_player, 0);
  if (pt->i_pointed + 1 < hand_size)
    {
      pt->i_pointed++;
      cardview_model_set_pointed (controller->model, pointed_hand,
				  pointed_player, pt->i_pointed);
    }
  else if (hand_size == 0)
    {
      cardview_model_unset_pointed (controller->model);
    }
  else
    {
      pt->i_pointed = 0;
      cardview_model_set_pointed (controller->model, pointed_hand,
				  pointed_player, pt->i_pointed);
    }
}

void
cardview_controller_point_backward (CardviewController * controller)
{
  size_t hand_size;
  PointedCard *pt;
  CardviewHandType pointed_hand;
  size_t pointed_player;
  size_t unused;
  cardview_model_get_pointed (controller->model, &pointed_hand,
			      &pointed_player, &unused);
  hand_size =
    cardview_model_size (controller->model, pointed_hand, pointed_player);
  pt =
    (PointedCard *) cardview_board_get (controller->pointed, pointed_hand,
					pointed_player, 0);
  if (pt->i_pointed != 0)
    {
      pt->i_pointed--;
      cardview_model_set_pointed (controller->model, pointed_hand,
				  pointed_player, pt->i_pointed);
    }
  else if (hand_size == 0)
    {
      cardview_model_unset_pointed (controller->model);
    }
  else
    {
      pt->i_pointed = hand_size - 1;
      cardview_model_set_pointed (controller->model, pointed_hand,
				  pointed_player, pt->i_pointed);
    }
}

void
cardview_controller_point_next (CardviewController * controller)
{
  CardviewHandType pointed_hand;
  size_t pointed_player;
  size_t unused;
  size_t n_players = cardview_board_n_players (controller->data);
  PointedCard *pt;
  cardview_model_get_pointed (controller->model, &pointed_hand,
			      &pointed_player, &unused);
  switch (pointed_hand)
    {
    case CARDVIEW_HAND_MAIN:
      pointed_hand = CARDVIEW_HAND_ADVERSARY;
      pointed_player = 0;
      if (pointed_player >= n_players)
	{
	  pointed_hand = CARDVIEW_HAND_TRICK;
	}
      break;
    case CARDVIEW_HAND_ADVERSARY:
      pointed_player++;
      if (pointed_player >= n_players)
	{
	  pointed_hand = CARDVIEW_HAND_TRICK;
	}
      break;
    case CARDVIEW_HAND_TRICK:
      pointed_hand = CARDVIEW_HAND_FOREGROUND;
      break;
    case CARDVIEW_HAND_FOREGROUND:
      pointed_hand = CARDVIEW_HAND_MAIN;
      break;
    }
  pt =
    (PointedCard *) cardview_board_get (controller->pointed, pointed_hand,
					pointed_player, 0);
  cardview_model_set_pointed (controller->model, pointed_hand, pointed_player,
			      pt->i_pointed);
}

void
cardview_controller_point_previous (CardviewController * controller)
{
  CardviewHandType pointed_hand;
  size_t pointed_player;
  size_t unused;
  size_t n_players = cardview_board_n_players (controller->data);
  PointedCard *pt;
  cardview_model_get_pointed (controller->model, &pointed_hand,
			      &pointed_player, &unused);
  switch (pointed_hand)
    {
      /* Reverse order of point_next */
    case CARDVIEW_HAND_MAIN:
      pointed_hand = CARDVIEW_HAND_FOREGROUND;
      break;
    case CARDVIEW_HAND_ADVERSARY:
      if (pointed_player == 0)
	{
	  pointed_hand = CARDVIEW_HAND_MAIN;
	}
      else
	{
	  pointed_player--;
	}
      break;
    case CARDVIEW_HAND_TRICK:
      pointed_hand = CARDVIEW_HAND_ADVERSARY;
      pointed_player = n_players;
      if (pointed_player == 0)
	{
	  pointed_hand = CARDVIEW_HAND_MAIN;
	}
      else
	{
	  pointed_player--;
	}
      break;
    case CARDVIEW_HAND_FOREGROUND:
      pointed_hand = CARDVIEW_HAND_TRICK;
      break;
    }
  pt =
    (PointedCard *) cardview_board_get (controller->pointed, pointed_hand,
					pointed_player, 0);
  cardview_model_set_pointed (controller->model, pointed_hand, pointed_player,
			      pt->i_pointed);
}

void
cardview_controller_toggle_selected (CardviewController * controller)
{
  CardviewHandType hand;
  size_t player;
  size_t i;
  if (cardview_controller_get_pointed (controller, &hand, &player, &i))
    {
      InnerType *datum;
      datum =
	(InnerType *) cardview_board_get (controller->data, hand, player, i);
      if (cardview_model_get_selected (controller->model, hand, player, i))
	{
	  cardview_model_set_selected (controller->model, hand, player, i, 0);
	}
      else if (datum->selectable)
	{
	  cardview_model_set_selected (controller->model, hand, player, i, 1);
	}
    }
}
