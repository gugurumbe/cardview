/*
 * controller.h
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_CARDVIEW_CONTROLLER_INCLUDED
#define H_CARDVIEW_CONTROLLER_INCLUDED

#include <stddef.h>
#include <cardview/model.h>

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */

#ifndef __GTK_DOC_IGNORE__
  struct CardviewController;
#endif

  /**
   * CardviewController:
   * The controller.
   */
  typedef struct CardviewController CardviewController;

  /**
   * cardview_controller_alloc:
   * 
   * Create a new controller.
   * n_players: the number of players.
   * n_cards: the number of cards per hand.
   * allocator: (value NULL): unused with glib.
   * Returns: a controller allocated with allocator.
   */
  CardviewController *cardview_controller_alloc (size_t n_players,
						 size_t n_cards,
						 CardviewAllocator *
						 allocator);

  /**
   * cardview_controller_free: (skip):
   *
   * Free a controller.
   * controller: the controller to free.
   */
  void cardview_controller_free (CardviewController * controller,
				 CardviewAllocator * allocator);

  /**
   * cardview_controller_copy: (skip):
   *
   * Copy a controller.
   * existing: the controller.
   * dest: an allocated controller for the copy.
   */
  void cardview_controller_copy (CardviewController * dest,
				 const CardviewController * controller);

  /**
   * cardview_controller_dup:
   *
   * Duplicate a controller.
   *
   * controller: the controller.
   * allocator: (value NULL): unused with GLib.
   * Returns: a copy.
   */
  CardviewController *cardview_controller_dup (const CardviewController *
					       controller,
					       CardviewAllocator * allocator);

  /**
   * cardview_controller_n_players:
   *
   * Get the number of players in the controller.
   *
   * controller: the controller.
   * Returns: the number of players in the controller.
   */
  size_t cardview_controller_n_players (const CardviewController *
					controller);

  /**
   * cardview_controller_max:
   *
   * Get the maximum number of cards per hand in the controller.
   *
   * controller: the controller.
   * Returns: the maximum number of cards per hand in the controller.
   */
  size_t cardview_controller_max (const CardviewController * controller);

  /**
   * cardview_controller_set_model:
   *
   * Define the model associated to the controller.
   *
   * controller: the controller.
   * model: the model.
   */
  void cardview_controller_set_model (CardviewController * controller,
				      const CardviewModel * model);

  /**
   * cardview_controller_set_selectable:
   *
   * Define whether a card can be selected.
   *
   * controller: the controller.
   * hand: which hand.
   * player: which adversary.
   * i: which card.
   * selectable: whether the card can be selected.
   */
  void cardview_controller_set_selectable (CardviewController * controller,
					   CardviewHandType hand,
					   size_t player, size_t i,
					   int selectable);

  /**
   * cardview_controller_get_pointed:
   *
   * Get the card that is pointed to.
   *
   * controller: the controller.
   * hand: which hand.
   * player: which player.
   * i: which card.
   * Returns: whether there is a pointed card.
   */
  int cardview_controller_get_pointed (const CardviewController * controller,
				       CardviewHandType * hand,
				       size_t * player, size_t * i);

  /**
   * cardview_controller_set_pointed:
   *
   * Define the card that is pointed to.
   *
   * controller: the controller.
   * hand: which hand.
   * player: which adversary.
   * i: which card.
   */
  void cardview_controller_set_pointed (CardviewController * controller,
					CardviewHandType hand, size_t player,
					size_t i);

  /**
   * cardview_controller_unset_pointed:
   *
   * Undefine the card that is pointed to.
   *
   * controller: the controller.
   */
  void cardview_controller_unset_pointed (CardviewController * controller);

  /**
   * cardview_controller_point_forward:
   *
   * Point to the next card in the same hand.
   *
   * controller: the controller.
   */
  void cardview_controller_point_forward (CardviewController * controller);

  /**
   * cardview_controller_point_backward:
   *
   * Point to the previous card in the same hand.
   *
   * controller: the controller.
   */
  void cardview_controller_point_backward (CardviewController * controller);

  /**
   * cardview_controller_point_next:
   *
   * Point to the next hand.
   *
   * controller: the controller.
   */
  void cardview_controller_point_next (CardviewController * controller);

  /**
   * cardview_controller_point_previous:
   *
   * Point to the previous hand.
   *
   * controller: the controller.
   */
  void cardview_controller_point_previous (CardviewController * controller);

  /**
   * cardview_controller_toggle_selected:
   *
   * Toggle the selection for the pointed card, if permitted by the
   * model.
   *
   * controller: the controller.
   */
  void cardview_controller_toggle_selected (CardviewController * controller);

#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif				/* H_CARDVIEW_CONTROLLER_INCLUDED */
