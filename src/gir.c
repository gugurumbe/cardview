/*
 * gir.c Load the symbols as gobjects.
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include "cardview.h"
#include <glib-object.h>

static void *
copy_model (void *data)
{
  return cardview_model_dup ((const CardviewModel *) data, NULL);
}

static void
free_model (void *data)
{
  CardviewModel *model = (CardviewModel *) data;
  cardview_model_free (model, NULL);
}

static void *
copy_model_iterator (void *data)
{
  CardviewModelIterator *iterator = (CardviewModelIterator *) data;
  return cardview_model_iterator_dup (iterator, NULL);
}

static void
free_model_iterator (void *data)
{
  CardviewModelIterator *iterator = (CardviewModelIterator *) data;
  cardview_model_iterator_free (iterator, NULL);
}

static void *
copy_controller (void *data)
{
  return cardview_controller_dup ((CardviewController *) data, NULL);
}

static void
free_controller (void *data)
{
  CardviewController *controller = (CardviewController *) data;
  cardview_controller_free (controller, NULL);
}

static void *
copy_board (void *data)
{
  return cardview_board_dup ((CardviewBoard *) data, NULL);
}

static void
free_board (void *data)
{
  CardviewBoard *board = (CardviewBoard *) data;
  cardview_board_free (board, NULL);
}

static void *
copy_board_iterator (void *data)
{
  return cardview_board_iterator_dup ((CardviewBoardIterator *) data, NULL);
}

static void
free_board_iterator (void *data)
{
  CardviewBoardIterator *it = (CardviewBoardIterator *) data;
  cardview_board_iterator_free (it, NULL);
}

static void *
copy_board_const_iterator (void *data)
{
  return cardview_board_const_iterator_dup ((CardviewBoardConstIterator *)
					    data, NULL);
}

static void
free_board_const_iterator (void *data)
{
  CardviewBoardConstIterator *it = (CardviewBoardConstIterator *) data;
  cardview_board_const_iterator_free (it, NULL);
}

/**
 * cardview_model_get_type:
 *
 * Register CardviewModel in glib.
 *
 * Returns: the type of a CardviewModel.
 */
GType cardview_model_get_type ();

/**
 * cardview_model_iterator_get_type:
 *
 * Register CardviewModelIterator in glib.
 *
 * Returns: the type of a CardviewModelIterator.
 */
GType cardview_model_iterator_get_type ();

/**
 * cardview_board_get_type:
 *
 * Register CardviewBoard in glib.
 *
 * Returns the type of a CardviewBoard.
 */
GType cardview_board_get_type ();

/**
 * cardview_board_iterator_get_type:
 *
 * Register CardviewBoardIterator in glib.
 *
 * Returns the type of a CardviewBoardIterator.
 */
GType cardview_board_iterator_get_type ();

/**
 * cardview_board_const_iterator_get_type:
 *
 * Register CardviewBoardConstIterator in glib.
 *
 * Returns the type of a CardviewBoardConstIterator.
 */
GType cardview_board_const_iterator_get_type ();

G_DEFINE_BOXED_TYPE (CardviewModel, cardview_model, copy_model, free_model)
G_DEFINE_BOXED_TYPE (CardviewModelIterator, cardview_model_iterator,
		     copy_model_iterator,
		     free_model_iterator) G_DEFINE_BOXED_TYPE (CardviewBoard,
							       cardview_board,
							       copy_board,
							       free_board)
G_DEFINE_BOXED_TYPE (CardviewController, cardview_controller, copy_controller,
		     free_controller)
G_DEFINE_BOXED_TYPE (CardviewBoardIterator, cardview_board_iterator,
		     copy_board_iterator,
		     free_board_iterator)
G_DEFINE_BOXED_TYPE (CardviewBoardConstIterator,
		     cardview_board_const_iterator, copy_board_const_iterator,
		     free_board_const_iterator)
