/*
 * loadgir.c Load the symbols exported in the gir.
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <girepository.h>
#include <stdio.h>
#include <stdlib.h>
#include <glib-object.h>

GType cardview_model_get_type ();
GType cardview_model_iterator_get_type ();
GType cardview_board_get_type ();
GType cardview_board_iterator_get_type ();
GType cardview_board_const_iterator_get_type ();

/* http://helgo.net/simon/introspection-tutorial/steptwo.xhtml */

int
main (int argc, char *argv[])
{
  GOptionContext *ctx;
  GError *error = NULL;
  cardview_model_get_type ();
  cardview_model_iterator_get_type ();
  cardview_board_get_type ();
  cardview_board_iterator_get_type ();
  cardview_board_const_iterator_get_type ();
  ctx = g_option_context_new (NULL);
  g_option_context_add_group (ctx, g_irepository_get_option_group ());
  if (!g_option_context_parse (ctx, &argc, &argv, &error)) {
    fprintf (stderr, "loadgir error: %s\n", error->message);
    return 1;
  }  
  return 0;
}
