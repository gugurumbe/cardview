/*
 * cardview.h Main cardview header.
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_CARDVIEW_INCLUDED
#define H_CARDVIEW_INCLUDED

#include <cardview/allocator.h>
#include <cardview/board.h>
#include <cardview/controller.h>
#include <cardview/model.h>
#include <cardview/scene.h>
#include <cardview/gtkwidget.h>
// #include <cardview/explorer.h>

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */

#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif				/* H_CARDVIEW_INCLUDED */
