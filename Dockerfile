FROM debian:stable
RUN apt-get update
RUN apt-get install -y --no-install-recommends texlive-base texlive-latex-base git ca-certificates texlive-fonts-recommended texlive-generic-recommended
RUN apt-get install -y --no-install-recommends autoconf automake gettext autopoint libtool texinfo gnulib autoconf-archive gobject-introspection
RUN apt-get install -y --no-install-recommends make gcc valac valac-vapi gtk-doc-tools flex bison
RUN apt-get install -y --no-install-recommends nettle-dev libgtk-3-dev libgirepository1.0-dev
RUN apt-get install -y --no-install-recommends emacs org-mode indent doxygen graphviz lcov dejagnu check
RUN apt-get install -y --no-install-recommends libxml2-utils xsltproc
RUN apt-get install -y --no-install-recommends guile-2.0-dev
RUN apt-get install -y --no-install-recommends wget
RUN apt-get install -y --no-install-recommends xcftools
RUN apt-get install -y --no-install-recommends gtk-doc-tools
RUN apt-get install -y --no-install-recommends inkscape
RUN apt-get upgrade -y
MAINTAINER framagit.org/gugurumbe/tarot