<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#org5c17944">1. READ ME</a></li>
<li><a href="#org62a667d">2. From git</a>
<ul>
<li><a href="#org97aaaf9">2.1. Gnulib</a></li>
<li><a href="#org46f038b">2.2. Gtk-doc</a></li>
<li><a href="#org92a3444">2.3. Autotools</a></li>
<li><a href="#orga782d0c">2.4. Prepare a distribution</a></li>
<li><a href="#org0bcfdba">2.5. <code>xcf2png</code></a></li>
</ul>
</li>
<li><a href="#orgbee8028">3. Project site</a></li>
<li><a href="#orgdcbcfdd">4. Bugs</a></li>
<li><a href="#org624826c">5. Roadmap</a></li>
<li><a href="#org079da42">6. Gtk-doc</a></li>
</ul>
</div>
</div>


<a id="org5c17944"></a>

# READ ME

This is the CardView project.  There is a detailed file named
`INSTALL` that cannot help you if you have just cloned the git repo.


<a id="org62a667d"></a>

# From git


<a id="org97aaaf9"></a>

## Gnulib

Run

    gnulib-tool --import mallog-gnu

in order to import a `malloc` function in case the one from the host
operating system has known bugs.  You can add more modules as you
wish.


<a id="org46f038b"></a>

## Gtk-doc

Since we document our API with gtk-doc, we have to use `gtk-docize`.

    gtkdocize


<a id="org92a3444"></a>

## Autotools

Run

    autoreconf --install

in order to import the build system.


<a id="orga782d0c"></a>

## Prepare a distribution

Run

    ./configure
    make dist

in order to have a full distribution.


<a id="org0bcfdba"></a>

## `xcf2png`

If you make a distribution from the git checkout, then you will need a
program named `xcf2png`, in order to convert the source of the images
(in the gimp format, `xcf`) to PNG.


<a id="orgbee8028"></a>

# Project site

The project site is [hosted online](http://gugurumbe.frama.io/cardview/share/doc/cardview/cardview.html/index.html).


<a id="orgdcbcfdd"></a>

# Bugs

Bugs, bugs, bugs everywhere!  I obviously did not see them, please
keep me informed: <vivien@planete-kraus.eu>.


<a id="org624826c"></a>

# Roadmap

Wohoo, we have cards, a semi-controller, that's great but&#x2026; it's
sooooo slow.  It takes, like, ages to select a card.

In fact, each **card** should be a widget, like an image widget.  I
think it would be much easier to handle, and much faster to render.
And we could have mouse pointer for free, if we found a way to tell
the widget that the collision order is fixed and does not depend on
the display order of the widget&#x2026;

The other problem is data files.  While the current system (a folder
for each deck size, and card images stacked in a row) works, it is not
extensible and not easy to work with.  We should have one PNG for each
card, and an XML to bind them all together with their index.  Thus we
could keep SVG cards as-is and let GTK+ draw the SVGS instead of
rasterizing.


<a id="org079da42"></a>

# Gtk-doc

Well, I have started building the gtk-doc, but it is definitely not
ready :-)

